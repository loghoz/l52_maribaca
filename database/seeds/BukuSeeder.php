<?php

use Illuminate\Database\Seeder;
use App\Buku;
use App\MateriBuku;
use App\Soal;
use App\Nilai;
use Faker\Factory as Faker;

class BukuSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        for ($i=1; $i < 11; $i++) { 
            Buku::create([
                'guru_id'                   => $faker->numberBetween($min = 2, $max = 19),
                'mata_pelajaran_id'         => $faker->numberBetween($min = 1, $max = 9),
                'tingkat_kelas'             => $faker->numberBetween($min = 10, $max = 12),
                'judul_buku'                => $faker->sentence($nbWords = 3, $variableNbWords = true),
                'photo'                     => 'cover_tkj.jpg',
                'tampil'                    => $faker->numberBetween($min = 40, $max = 100),
            ]);
            for ($j=1; $j < 5; $j++) { 
                MateriBuku::create([
                    'buku_id'        => $i,
                    'materi'         => 'Materi '.$j,
                    'isi_materi'     => $faker->text($maxNbChars = 3000),
                ]);
            }
            for ($k=1; $k < 5; $k++) { 
                Soal::create([
                    'buku_id'       => $i,
                    'soal'          => $faker->text($maxNbChars = 1000),
                    'jawaban_a'     => $faker->text($maxNbChars = 100),
                    'jawaban_b'     => $faker->text($maxNbChars = 100),
                    'jawaban_c'     => $faker->text($maxNbChars = 100),
                    'jawaban_d'     => $faker->text($maxNbChars = 100),
                    'jawaban_benar' => 'A',
                    'photo_soal'    => '-',
                    'photo_a'       => '-',
                    'photo_b'       => '-',
                    'photo_c'       => '-',
                    'photo_d'       => '-',
                ]);
            }
            for ($l=1; $l < 19; $l++) { 
                Nilai::create([
                    'buku_id'           => $i,
                    'siswa_id'          => $l,
                    'nilai'             => $faker->numberBetween($min = 1, $max = 100),
                ]);
            }
        }
    }
}
