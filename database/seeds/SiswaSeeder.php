<?php

use Illuminate\Database\Seeder;
use App\Siswa;
use Faker\Factory as Faker;

class SiswaSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();
        for ($i=1; $i < 10; $i++) { 
            Siswa::create([
                'no_induk'      => '111'.$i,
                'nama'          => $faker->name($gender = 'male'),
                'jk'            => 'L',
                'telp'          =>'+6289631073926',
                'photo'         =>'avatar1.png',
                'hapus'         => '0',
            ]);
        }
        

        for ($i=1; $i < 10; $i++) { 
            Siswa::create([
                'no_induk'      => '222'.$i,
                'nama'          => $faker->name($gender = 'female'),
                'jk'            => 'P',
                'telp'          => '+6289631073926',
                'photo'         => 'avatar3.png',
                'hapus'         => '0',
            ]);
        }
    }
}
