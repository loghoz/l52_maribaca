<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //primary data
        $this->call(TahunAjaranSeeder::class);
        $this->call(JurusanSeeder::class);
        $this->call(KelasSeeder::class);

        //optional
        $this->call(MataPelajaranSeeder::class);
        $this->call(GuruSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(SiswaSeeder::class);
        $this->call(BukuSeeder::class);
    }
}
