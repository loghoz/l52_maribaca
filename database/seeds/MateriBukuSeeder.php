<?php

use Illuminate\Database\Seeder;
use App\MateriBuku;
use Faker\Factory as Faker;

class MateriBukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker::create();

        for ($i=1; $i < 11; $i++) { 
            MateriBuku::create([
                'buku_id'        => $faker->numberBetween($min = 2, $max = 19),
                'materi'         => $faker->numberBetween($min = 1, $max = 9),
                'isi_materi'     => $faker->numberBetween($min = 10, $max = 12),
            ]);
        }
    }
}
