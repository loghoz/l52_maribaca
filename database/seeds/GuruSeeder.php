<?php

use Illuminate\Database\Seeder;
use App\Guru;
use Faker\Factory as Faker;

class GuruSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();

        //default
        Guru::create([
            'no_induk'      => '999999',
            'nama'          => '-',
            'jk'            => 'L',
            'telp'          => '-',
            'photo'         =>'avatar5.png',
            'tahun_ajaran'  => '-',
            'hapus'         => '0',
        ]);
        
        for ($i=1; $i < 10; $i++) { 
            Guru::create([
                'no_induk'      => '11111'.$i,
                'nama'          => $faker->name($gender = 'male'),
                'jk'            => 'L',
                'telp'          => '6432423',
                'photo'         => 'avatar5.png',
                'hapus'         => '0',
            ]);
        }

        for ($i=1; $i < 10; $i++) { 
            Guru::create([
                'no_induk'      => '22222'.$i,
                'nama'          => $faker->name($gender = 'female'),
                'jk'            => 'P',
                'telp'          => '6432423',
                'photo'         => 'avatar2.png',
                'hapus'         => '0',
            ]);
        }
    }
}
