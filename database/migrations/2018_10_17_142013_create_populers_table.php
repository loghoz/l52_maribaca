<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopulersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('populers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buku_id')->unsigned();
            $table->integer('populer');
            $table->timestamps();

            $table->foreign('buku_id')
                ->references('id')
                ->on('bukus')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('populers');
    }
}
