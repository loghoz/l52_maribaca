<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGurusTable extends Migration
{

    public function up()
    {
        Schema::create('gurus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_induk');
            $table->string('nama');
            $table->string('jk');
            $table->string('telp');
            $table->string('photo');
            $table->string('tahun_ajaran');
            $table->string('hapus');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('gurus');
    }
}
