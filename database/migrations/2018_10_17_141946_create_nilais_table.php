<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('siswa_id')->unsigned();
            $table->integer('buku_id')->unsigned();
            $table->string('nilai');
            $table->timestamps();

            $table->foreign('siswa_id')
                ->references('id')
                ->on('siswas')
                ->onDelete('CASCADE');

            $table->foreign('buku_id')
                ->references('id')
                ->on('bukus')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nilais');
    }
}
