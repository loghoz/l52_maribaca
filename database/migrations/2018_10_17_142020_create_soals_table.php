<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buku_id')->unsigned();
            $table->text('soal');
            $table->text('jawaban_a');
            $table->text('jawaban_b');
            $table->text('jawaban_c');
            $table->text('jawaban_d');
            $table->string('jawaban_benar');
            $table->string('photo_soal');
            $table->string('photo_a');
            $table->string('photo_b');
            $table->string('photo_c');
            $table->string('photo_d');
            $table->timestamps();

            $table->foreign('buku_id')
                ->references('id')
                ->on('bukus')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soals');
    }
}
