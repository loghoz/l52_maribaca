<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'bukus';

    protected $fillable = [
        'guru_id','mata_pelajaran_id','tingkat_kelas','judul_buku','photo','tampil',
    ];

    // relasi ke guru
    public function guru()
    {
        return $this->belongsTo('App\Guru', 'guru_id');
    }

    // relasi ke mapel
    public function mapel()
    {
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajaran_id');
    }

    // relasi one to many ke soal
    public function soal()
    {
        return $this->hasMany('App\Soal');
    }
    
    // relasi one to many ke materi
    public function materi()
    {
        return $this->hasMany('App\MateriBuku');
    }

    // relasi one to many ke nilai
    public function nilai()
    {
        return $this->hasMany('App\Nilai');
    }

    //relasi one to one ke populer
    public function populer()
    {
        return $this->hasOne('App\Populer');
    }
}
