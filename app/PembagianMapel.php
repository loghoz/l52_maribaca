<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembagianMapel extends Model
{
    
    protected $table = 'pembagian_mapels';

    protected $fillable = [
        'jurusan_id','mata_pelajaran_id','tahun_ajaran_id',
    ];

    //relasi one to one ke buku
    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan','jurusan_id');
    }

    //relasi one to one ke siswa
    public function mapel()
    {
        return $this->belongsTo('App\MataPelajaran','mata_pelajaran_id');
    }
    
    //relasi one to one ke siswa
    public function ta()
    {
        return $this->belongsTo('App\TahunAjaran','tahun_ajaran_id');
    }
}
