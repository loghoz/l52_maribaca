<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembagianKelas extends Model
{
    protected $table = 'pembagian_kelas';

    protected $fillable = [
        'siswa_id','kelas_id','tahun_ajaran_id',
    ];

    //relasi one to one ke buku
    public function kelas()
    {
        return $this->belongsTo('App\Kelas','kelas_id');
    }

    //relasi one to one ke siswa
    public function siswa()
    {
        return $this->belongsTo('App\Siswa','siswa_id');
    }
    
   //relasi one to one ke siswa
   public function ta()
   {
       return $this->belongsTo('App\TahunAjaran','tahun_ajaran_id');
   }
}
