<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Populer extends Model
{
    protected $table = 'populers';

    protected $fillable = [
        'buku_id','nilai',
    ];

    //relasi one to one ke buku
    public function buku()
    {
        return $this->belongsTo('App\Buku','buku_id');
    }
}
