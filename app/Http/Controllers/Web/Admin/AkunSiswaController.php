<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
class AkunSiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {

        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }

        $akun = User::where('akses','siswa')
            ->paginate(10);

        return view('admin.akun.akun_siswa', compact('akun', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
    
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $akun = User::select('no_induk','id')->findOrFail($id);

        return view('admin.akun.edit_siswa', compact('akun'));
    }

    public function update(Request $request, $id)
    {
        $akun = User::findOrFail($id);

        $password = $request->get('password');

        if ($password == '') {
            $data['password'] = $akun->password;
        } else {
            $data['password'] = bcrypt($request->get('password'));
        }

        $akun->update($data);

        $notification = array(
            'message' => 'Password Siswa dengan No Induk '.$akun->no_induk.' telah diubah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.akun.siswa.index')->with($notification);
    }

    public function destroy($id)
    {
        
    }
}
