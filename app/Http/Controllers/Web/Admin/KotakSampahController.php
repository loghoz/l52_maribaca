<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Siswa;
use App\Guru;

use DB;
class KotakSampahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');
        $q = $request->get('q');
        $r = $request->get('r');
        $kelas_id = $request->get('kelas_id');
        $page = $request->get('page');
        $no = 1;

        if($page>1){
            $no = $page * 12 - 11;
        }else{
            $no=1;
        }

        $siswa = Siswa::where('nama', 'LIKE', '%'.$q.'%')
            ->where('kelas_id', 'LIKE', '%'.$kelas_id.'%')
            ->where('tahun_ajaran', $tahun_ajaran)
            ->where('hapus', '1')
            ->orderBy('kelas_id','asc')
            ->orderBy('nama','asc')
            ->paginate(12);

        $guru = Guru::where('no_induk', 'NOT LIKE', '999999')
            ->where('nama', 'LIKE', '%'.$r.'%')
            ->where('hapus', '1')
            ->orderBy('nama','asc')
            ->paginate(12);

        return view('admin.kotak.sampah', compact('siswa','guru','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
