<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\GuruRequest;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\User;
use App\Guru;

use DB;

class GuruController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {
        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 15 - 14;
        }else{
            $no=1;
        }

        $guru = Guru::where('no_induk', 'NOT LIKE', '999999')
            ->where('nama', 'LIKE', '%'.$q.'%')
            ->where('hapus', '0')
            ->orderBy('nama','asc')
            ->paginate(15);

        return view('admin.guru.guru', compact('guru', 'no' ,'q'));
    }

    public function create()
    {
        return view('admin.guru.tambah_guru');
    }

    public function store(GuruRequest $request)
    {
        // SIMPAN DATA GURU
        $dataGuru = $request->only('no_induk','nama','jk','telp','photo','hapus');

        if ($request->hasFile('photo')) {
            $dataGuru['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            if($request->jk == 'L'){
              $dataGuru['photo'] = 'avatar5.png';
            } else {
              $dataGuru['photo'] = 'avatar2.png';
            }
        }

        Guru::create($dataGuru);

        // SIMPAN DATA USER GURU
        User::create([
            'no_induk'  => $request->no_induk,
            'no_induk'  => $request->email,
            'password'  => bcrypt('smkp3l1t4'),
            'api_token' => bcrypt($request->no_induk),
            'akses'      => 'guru'
        ]);

        $notification = array(
            'message' => 'Guru dengan nama '.$request->get('nama').' telah ditambahkan.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.guru.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $guru = Guru::findOrFail($id);

        return view('admin.guru.edit_guru', compact('guru'));
    }

    public function update(GuruRequest $request, $id)
    {
        // SIMPAN DATA GURU
        $guru = Guru::findOrFail($id);
        $user = User::where('no_induk',$guru->no_induk);

        $dataGuru = $request->only('no_induk','nama','jk','telp','hapus');

        if ($request->hasFile('photo')) {
            $dataGuru['photo'] = $this->savePhoto($request->file('photo'));
            if (($guru->photo == 'avatar2.png')||($guru->photo == 'avatar5.png')) {

            } else {
                $this->deletePhoto($guru->photo);
            }
        } else {
            if($guru->photo !== ('avatar2.png')||('avatar5.png')) {
                $dataGuru['photo'] = $guru->photo;
            } elseif ($request->jk == 'P') {
                $dataGuru['photo'] = 'avatar5.png';
            } else {
                $dataGuru['photo'] = 'avatar2.png';
            }
        }

        $guru->update($dataGuru);

        $dataUser = $request->only('no_induk','email');
        $user->update($dataUser);

        $notification = array(
            'message' => 'Data Guru dengan nama '.$request->get('nama').' telah diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.guru.index')->with($notification);
    }

    // HAPUS DATA GURU SEMENTARA
    public function hapus($id)
    {
        $hapus = Guru::findOrFail($id);
        DB::update('update gurus set hapus = "1" where id = '.$id);

        $notification = array(
            'message' => 'Data Guru dengan nama '.$hapus->nama.' telah dihapus.',
            'alert-type' => 'warning'
        );
        return redirect()->route('admin.guru.index')->with($notification);
    }

    // KEMBALIKAN DATA GURU SEMENTARA
    public function balikin($id)
    {
        $hapus = Guru::findOrFail($id);
        DB::update('update gurus set hapus = "0" where id = '.$id);
        $notification = array(
            'message' => 'Data Guru dengan nama '.$hapus->nama.' telah dikembalikan.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.sampah.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = Guru::findOrFail($id);
        $notification = array(
            'message' => 'Data Guru dengan nama '.$hapus->nama.' telah dihapus permanen.',
            'alert-type' => 'error'
        );
        $guru       = Guru::findOrFail($id);

        if ($guru->photo !== ('avatar2.png')&&('avatar5.png')) $this->deletePhoto($guru->photo);
        
        //delete user
        $user = User::where('no_induk',$guru->no_induk);
        $user->delete();

        //delete guru
        $guru->delete();

        return redirect()->route('admin.guru.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'itlabil/images/guru';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/images/guru/'. $filename;
      return File::delete($path);
    }
}
