<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\JurusanRequest;
use App\Http\Controllers\Controller;

use App\Jurusan;

class JurusanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }

        $jurusan = Jurusan::where('kode_jurusan','not like','-')->paginate(10);
        return view('admin.jurusan.jurusan', compact('jurusan','no'));
    }

    public function create()
    {
        //
    }

    public function store(JurusanRequest $request)
    {
        Jurusan::create($request->all());
        $notification = array(
            'message' => 'Jurusan '.$request->get('kode_jurusan').' telah ditambahkan.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.jurusan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
      $jurusan = Jurusan::findOrFail($id);
      return view('admin.jurusan.edit_jurusan', compact('jurusan'));
    }

    public function update(JurusanRequest $request, $id)
    {
        $jurusan = Jurusan::findOrFail($id);
        $notification = array(
            'message' => 'Jurusan '.$request->get('kode_jurusan').' telah diubah.',
            'alert-type' => 'info'
        );
        $jurusan->update($request->all());
        return redirect()->route('admin.jurusan.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = Jurusan::findOrFail($id);
        $notification = array(
            'message' => 'Jurusan '.$hapus->kode_jurusan.' telah dihapus.',
            'alert-type' => 'error'
        );
        Jurusan::find($id)->delete();
        return redirect()->route('admin.jurusan.index')->with($notification);
    }
}
