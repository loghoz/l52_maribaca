<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\MapelNaRequest;
use App\Http\Controllers\Controller;

use App\MataPelajaran;

class MataPelajaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {
      $tahun_ajaran = $request->cookie('tahun_ajaran');

      $page = $request->get('page');

      $no = 1;

      if($page>1){
          $no = $page * 10 - 9;
      }else{
          $no=1;
      }

      $mapel = MataPelajaran::where('jenis_mat_pel','Normatif dan Adaptif')
                            ->orderBy('kode_mat_pel','asc')
                            ->paginate(5);

      return view('admin.mapel.na', compact('mapel', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(MapelNaRequest $request)
    {
        MataPelajaran::create($request->all());

        $notification = array(
            'message' => 'Mata Pelajaran '.$request->get('mata_pelajaran').' telah ditambahkan.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.mapel.na.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $mapel = MataPelajaran::findOrFail($id);

        return view('admin.mapel.edit_mapel_na', compact('mapel'));
    }

    public function update(MapelNaRequest $request, $id)
    {
        $mapel = MataPelajaran::findOrFail($id);

        $mapel->update($request->all());

        $notification = array(
            'message' => 'Mata Pelajaran '.$request->get('mata_pelajaran').' telah diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.mapel.na.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = MataPelajaran::findOrFail($id);
        $notification = array(
            'message' => 'Mata Pelajaran '.$hapus->mata_pelajaran.' telah dihapus.',
            'alert-type' => 'error'
        );
        MataPelajaran::find($id)->delete();

        return redirect()->route('admin.mapel.na.index')->with($notification);
    }
}
