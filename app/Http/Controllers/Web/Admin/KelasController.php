<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\KelasRequest;
use App\Http\Controllers\Controller;

// model Kelas
use App\Kelas;
use App\Jurusan;

class KelasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

  public function index(Request $request)
    {
        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }

        $kelas = Kelas::where('kode_kelas', 'LIKE', '%'.$q.'%')
        ->orderBy('kode_kelas','asc')
        ->paginate(10);

        $jurusan = Jurusan::lists('jurusan', 'id');

        return view('admin.kelas.kelas', compact('kelas','no', 'q', 'jurusan'));
    }

    public function create()
    {
        //
    }

    public function store(KelasRequest $request)
    {
        Kelas::create($request->all());
        $notification = array(
            'message' => 'Kelas '.$request->get('kode_kelas').' telah ditambahkan.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.kelas.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jurusan = Jurusan::lists('jurusan', 'id');
        $kelas = Kelas::findOrFail($id);

        return view('admin.kelas.edit_kelas', compact('kelas', 'jurusan'));
    }

    public function update(KelasRequest $request, $id)
    {
        $kelas = Kelas::findOrFail($id);

        $kelas->update($request->all());
        $notification = array(
            'message' => 'Kelas '.$request->get('kode_kelas').' telah diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.kelas.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = Kelas::findOrFail($id);
        $notification = array(
            'message' => 'Kelas '.$hapus->kode_kelas.' telah dihapus.',
            'alert-type' => 'error'
        );
        Kelas::find($id)->delete();
        return redirect()->route('admin.kelas.index')->with($notification);
    }
}
