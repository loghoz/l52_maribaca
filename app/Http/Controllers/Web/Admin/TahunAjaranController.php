<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\TahunAjaran;
use App\Guru;
use App\Siswa;

class TahunAjaranController extends Controller
{
    public function index()
    {
        $ta = TahunAjaran::lists('tahun_ajaran', 'tahun_ajaran');
        
        return view('auth.tahun', compact('ta'));
    }

    public function create()
    {
        //
    }

    public function store()
    {
        //
    }

    public function coota()
    {
        $role = Auth::user()->akses;
        $n_i = Auth::user()->no_induk;
        
        if ($role=='guru') {
            $guru = Guru::where('no_induk',$n_i)->first();
                    
            if (empty($guru)) {
                return redirect('/guru/beranda');
            }else{
                $photo = cookie()->forever('photo', $guru->photo);
                $nama = cookie()->forever('nama', $guru->nama);

                return redirect()->route('guru.beranda.index')
                ->withCookie($photo)
                ->withCookie($nama);
            }

        } elseif ($role=='siswa') {
            $siswa = Siswa::where('no_induk',$n_i)->first();
            
            if (empty($siswa)) {
                return redirect('/siswa/beranda');
            }else{
                $photo = cookie()->forever('photo', $siswa->photo);
                $nama = cookie()->forever('nama', $siswa->nama);
        
                return redirect()->route('siswa.beranda.index')
                ->withCookie($photo)
                ->withCookie($nama);
            }
        } else {
            $nama = cookie()->forever('nama', 'Admin');
            $photo = cookie()->forever('photo', 'avatar4.png');
        
            return redirect()->route('home.index')
            ->withCookie($photo)
            ->withCookie($nama);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $ta = TahunAjaran::findOrFail($id);
        
        return view('admin.pengaturan.edit_ta', compact('ta'));
    }

    public function update(Request $request, $id)
    {
        $ta = TahunAjaran::findOrFail($id);
        $data = $request->only('tahun_ajaran');
        $ta->update($data);
        return redirect()->route('admin.pengaturan.index');
    }

    public function destroy($id)
    {
        TahunAjaran::find($id)->delete();
  
        return redirect()->route('admin.pengaturan.index');
    }

    public function tambah(Request $request)
    {
        TahunAjaran::create($request->all());
        return redirect()->route('admin.pengaturan.index');
    }
}
