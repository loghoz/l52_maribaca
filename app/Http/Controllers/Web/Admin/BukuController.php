<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BukuRequest;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Buku;
use App\Nilai;
use App\Soal;
use App\Guru;
use App\MataPelajaran;
use App\Populer;

use DB;

class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {

        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 15 - 14;
        }else{
            $no=1;
        }

        $buku = Buku::where('judul_buku', 'LIKE', '%'.$q.'%')
            ->orderBy('id','desc')
            ->paginate(12);

        return view('admin.buku.buku', compact('buku', 'no' ,'q'));
    }

    public function create()
    {
        $guru = Guru::lists('nama', 'id');
        $mapel = MataPelajaran::orderBy('jurusan_id','asc')
                        ->orderBy('mata_pelajaran','asc')
                        ->lists('mata_pelajaran', 'id');

        return view('admin.buku.tambah_buku',compact('guru','mapel'));
    }

    public function store(BukuRequest $request)
    {
        $dataBuku = $request->only('guru_id','mata_pelajaran_id','tingkat_kelas','judul_buku');
        // $dataBuku['buku'] = $this->savePdf($request->file('buku'));

        $mapel = MataPelajaran::where('id',$request->mata_pelajaran_id)->get()->first();

        if ($request->hasFile('photo')) {
            $dataBuku['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            if ($mapel->jurusan_id=='2') {
                $dataBuku['photo'] = 'cover_ap.jpg';
            } else if ($mapel->jurusan_id=='3') {
                $dataBuku['photo'] = 'cover_ak.jpg';
            } else if ($mapel->jurusan_id=='4') {
                $dataBuku['photo'] = 'cover_pms.jpg';
            } else if ($mapel->jurusan_id=='5') {
                $dataBuku['photo'] = 'cover_tkj.jpg';
            } else if ($mapel->jurusan_id=='6') {
                $dataBuku['photo'] = 'cover_mm.jpg';
            } else if ($mapel->jurusan_id=='7') {
                $dataBuku['photo'] = 'cover_fm.jpg';
            } else {
                $dataBuku['photo'] = 'cover_na.jpg';
            }
        }
        $dataBuku['tampil'] = 0;

        Buku::create($dataBuku);

        $notification = array(
            'message' => 'Buku '.$request->get('judul_buku').' telah ditambahkan.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.buku.index')->with($notification);
    }

    public function show($id)
    {
        $buku = Buku::findOrFail($id);
        $path = 'itlabil/images/buku/'.$buku->buku;
        return response()->file($path);
    }

    public function tambahview($id)
    {
        $buku = Buku::findOrFail($id);
        $dataBuku['tampil'] = $buku->tampil+1;
        $buku->update($dataBuku);
        
        return redirect('admin/buku/'.$id);
    }

    public function edit($id)
    {
        $guru = Guru::lists('nama', 'id');
        $buku = Buku::findOrFail($id);

        $mapel = MataPelajaran::orderBy('jurusan_id','asc')
                        ->orderBy('mata_pelajaran','asc')
                        ->lists('mata_pelajaran', 'id');

        return view('admin.buku.edit_buku', compact('buku','guru','mapel'));
    }

    public function update(Request $request, $id)
    {
        $buku = Buku::findOrFail($id);

        $dataBuku = $request->only('guru_id','mata_pelajaran_id','tingkat_kelas','judul_buku');

        if ($request->hasFile('photo')) {
            if ($buku->photo=="cover_ap.jpg"||$buku->photo=="cover_ak.jpg"||$buku->photo=="cover_pms.jpg"||$buku->photo=="cover_tkj.jpg"||$buku->photo=="cover_mm.jpg"||$buku->photo=="cover_fm.jpg"||$buku->photo=="cover_na.jpg") {
                $dataBuku['photo'] = $this->savePhoto($request->file('photo'));
            } else {
                $this->deletePhoto($buku->photo);
                $dataBuku['photo'] = $this->savePhoto($request->file('photo'));
            }
        }

        // if ($request->hasFile('buku')) {
        //     $this->deletePhoto($buku->buku);
        //     $dataBuku['buku'] = $this->savePdf($request->file('buku'));
        // }
        
        $buku->update($dataBuku);
        $notification = array(
            'message' => 'Buku '.$request->get('judul_buku').' telah diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.buku.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = Buku::findOrFail($id);
        $notification = array(
            'message' => 'Buku dengan judul '.$hapus->judul_buku.' telah dihapus.',
            'alert-type' => 'error'
        );

        $buku = Buku::findOrFail($id);

        if ($buku->photo !== ($buku->photo=="cover_ap.jpg"||$buku->photo=="cover_ak.jpg"||$buku->photo=="cover_pms.jpg"||$buku->photo=="cover_tkj.jpg"||$buku->photo=="cover_mm.jpg"||$buku->photo=="cover_fm.jpg"||$buku->photo=="cover_na.jpg")) $this->deletePhoto($buku->photo);
        
        // $this->deletePdf($buku->buku);

        //delete soal
        $soal = Soal::where('buku_id',$buku->id);
        $soal->delete();

        //delete nilai
        $nilai = Nilai::where('buku_id',$buku->id);
        $nilai->delete();

        //delete guru
        $buku->delete();

        return redirect()->route('admin.buku.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'itlabil/images/cover';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/images/cover/'. $filename;
      return File::delete($path);
    }

    protected function savePdf(UploadedFile $pdf)
    {
      $fileName = str_random(40) . '.' . $pdf->guessClientExtension();
      $destinationPath = 'itlabil/images/buku';
      $pdf->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePdf($filename)
    {
      $path = 'itlabil/images/buku/'. $filename;
      return File::delete($path);
    }
}
