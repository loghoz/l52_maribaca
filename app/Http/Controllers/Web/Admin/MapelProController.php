<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\MapelProRequest;
use App\Http\Controllers\Controller;

use App\MataPelajaran;
use App\Jurusan;

class MapelProController extends Controller
{
    public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('akses:admin');
  }

  public function index(Request $request)
  {
    $tahun_ajaran = $request->cookie('tahun_ajaran');

    $page = $request->get('page');

    $no = 1;

    if($page>1){
        $no = $page * 10- 9;
    }else{
        $no=1;
    }

    $mapel = MataPelajaran::where('jenis_mat_pel','Produktif')
      ->orderBy('kode_mat_pel','asc')->paginate(10);

    $jurusan = Jurusan::lists('jurusan', 'id');

    return view('admin.mapel.pro', compact('mapel', 'no','jurusan'));
  }

  public function create()
  {
      //
  }

  public function store(MapelProRequest $request)
  {
      MataPelajaran::create($request->all());

      $notification = array(
        'message' => 'Mata Pelajaran '.$request->get('mata_pelajaran').' telah ditambahkan.',
        'alert-type' => 'success'
    );
      return redirect()->route('admin.mapel.pro.index')->with($notification);
  }

  public function show($id)
  {
      //
  }

  public function edit($id)
  {
    $mapel = MataPelajaran::findOrFail($id);
    $jurusan = Jurusan::lists('jurusan', 'id');

    return view('admin.mapel.edit_mapel_pro', compact('mapel','jurusan'));
  }

  public function update(MapelProRequest $request, $id)
  {
    $mapel = MataPelajaran::findOrFail($id);

    $mapel->update($request->all());

    $notification = array(
        'message' => 'Mata Pelajaran '.$request->get('mata_pelajaran').' telah diubah.',
        'alert-type' => 'info'
    );
    return redirect()->route('admin.mapel.pro.index')->with($notification);
  }

  public function destroy($id)
  {
    $hapus = MataPelajaran::findOrFail($id);
    $notification = array(
        'message' => 'Mata Pelajaran '.$hapus->mata_pelajaran.' telah dihapus.',
        'alert-type' => 'error'
    );
    MataPelajaran::find($id)->delete();

    return redirect()->route('admin.mapel.pro.index')->with($notification);
  }
}
