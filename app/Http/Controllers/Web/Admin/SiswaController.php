<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateSiswaRequest;
use App\Http\Requests\SiswaRequest;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Siswa;
use App\User;
use App\Kelas;
use App\TahunAjaran;

use Alert;
use DB;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {
        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 15 - 14;
        }else{
            $no=1;
        }

        $kelas = Kelas::lists('kode_kelas', 'id');

        $siswa = Siswa::where('nama', 'LIKE', '%'.$q.'%')
            ->where('hapus', '0')
            ->orderBy('nama','asc')
            ->paginate(15);

        return view('admin.siswa.siswa', compact('siswa', 'no' ,'q'));
    }

    public function create(Request $request)
    {
        return view('admin.siswa.tambah_siswa');
    }

    public function store(CreateSiswaRequest $request)
    {
        // SIMPAN DATA SISWA
        $dataSiswa = $request->only('jk','no_induk','nama','telp','hapus');
        
        if ($request->hasFile('photo')) {
            $dataSiswa['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            if($request->jk == 'L'){
              $dataSiswa['photo'] = 'avatar1.png';
            } else {
              $dataSiswa['photo'] = 'avatar3.png';
            }
        }

        Siswa::create($dataSiswa);

        // SIMPAN DATA USER
        User::create([
            'no_induk'  => $request->no_induk,
            'email'     => $request->email,
            'password'  => bcrypt('smkpelita'),
            'api_token' => bcrypt($request->no_induk),
            'akses'      => 'siswa'
        ]);

        $notification = array(
            'message' => 'Siswa dengan nama '.$request->get('nama').' telah ditambahkan.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.siswa.index')->with($notification);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
  
        $siswa      = Siswa::findOrFail($id); 
  
        return view('admin.siswa.edit_siswa', compact('siswa'));
    }

    public function update(SiswaRequest $request, $id)
    {
        // SIMPAN DATA SISWA
        $siswa = Siswa::findOrFail($id);
        $user = User::where('no_induk',$siswa->no_induk);

        $dataSiswa = $request->only('no_induk','nama','jk','telp','hapus');

        if ($request->hasFile('photo')) {
            $dataSiswa['photo'] = $this->savePhoto($request->file('photo'));
            if (($siswa->photo == 'avatar1.png')||($siswa->photo == 'avatar3.png')) {

            } else {
              $this->deletePhoto($siswa->photo);
            }
        } else {
            if($siswa->photo !== ('avatar1.png')||('avatar3.png')) {
              $dataSiswa['photo'] = $siswa->photo;
            } elseif ($request->jk == 'L') {
              $dataSiswa['photo'] = 'avatar1.png';
            } else {
              $dataSiswa['photo'] = 'avatar3.png';
            }
        }

        $siswa->update($dataSiswa);

        $dataUser = $request->only('no_induk','email');
        $user->update($dataUser);
        
        $notification = array(
            'message' => 'Data Siswa dengan nama '.$request->get('nama').' telah diubah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.siswa.index')->with($notification);
    }

    // HAPUS DATA SISWA SEMENTARA
    public function hapus($id)
    {
        $hapus = Siswa::findOrFail($id);
  
        DB::update('update siswas set hapus = "1" where id = '.$id);

        $notification = array(
            'message' => 'Data Siswa dengan nama '.$hapus->nama.' telah dihapus.',
            'alert-type' => 'warning'
        );

        return redirect()->route('admin.siswa.index')->with($notification);
    }

    // KEMBALIKAN DATA SISWA SEMENTARA
    public function balikin($id)
    {
        $hapus = Siswa::findOrFail($id);

        DB::update('update siswas set hapus = "0" where id = '.$id);
        $notification = array(
            'message' => 'Data Siswa dengan nama '.$hapus->nama.' telah dikembalikan.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.sampah.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = Siswa::findOrFail($id);

        $notification = array(
            'message' => 'Data Siswa dengan nama '.$hapus->nama.' telah dihapus permanen.',
            'alert-type' => 'error'
        );

        //delete siswa
        $siswa      = Siswa::find($id);
  
        if ($siswa->photo !== ('avatar1.png')&&('avatar3.png')) $this->deletePhoto($siswa->photo);
  
        //delete siswa
        $siswa->delete();
  
        //delete user
        $user = User::where('no_induk',$siswa->no_induk);
        $user->delete();
  
        return redirect()->route('admin.siswa.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'itlabil/images/siswa';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/images/siswa/'.$filename;
      return File::delete($path);
    }
}
