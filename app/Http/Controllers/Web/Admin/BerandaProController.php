<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Siswa;
use App\Guru;
use App\Buku;

class BerandaProController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {
        $page = $request->get('page');
        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }
        $jml_guru = Guru::where('no_induk', 'NOT LIKE', '999999')->where('hapus', '0')->count();
        $jml_siswa = Siswa::where('hapus', '0')->count();

        $k_siswa = Siswa::where('hapus', '1')->count();
        $k_guru = Guru::where('no_induk', 'NOT LIKE', '999999')->where('hapus', '1')->count();
        $jml_k = $k_siswa+$k_guru;

        $buku = Buku::orderBy('tampil','desc')->limit(10)->get();

        return view('admin.beranda.beranda',compact('jml_guru','jml_siswa','jml_k','no','buku'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
