<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AkunRequest;
use App\Http\Requests\AkunEditRequest;
use App\Http\Controllers\Controller;

use App\User;
class AkunController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:admin');
    }

    public function index(Request $request)
    {

        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }

        $akun = User::where('akses','admin')
            ->paginate(10);

        return view('admin.akun.akun_admin', compact('akun', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data               = $request->only('no_induk');
        $data['akses']      = 'admin';
        $data['email']      = $request->get('no_induk').'@smkpelitapesawaran.sch.id';
        $data['api_token']  = bcrypt($request->get('no_induk').'@smkpelitapesawaran.sch.id');
        $data['password']   = bcrypt($request->get('password'));

        User::create($data);

        $notification = array(
            'message' => 'Admin dengan No Induk '.$request->get('no_induk').' telah ditambahkan.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.akun.admin.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $akun = User::select('no_induk','id')->findOrFail($id);

        return view('admin.akun.edit_admin', compact('akun'));
    }

    public function update(Request $request, $id)
    {
        $akun = User::findOrFail($id);

        $password = $request->get('password');

        if ($password == '') {
            $data['password'] = $akun->password;
        } else {
            $data['password'] = bcrypt($request->get('password'));
        }

        $akun->update($data);

        $notification = array(
            'message' => 'Password Admin dengan No Induk '.$akun->no_induk.' telah diubah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.akun.admin.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = User::findOrFail($id);
        $notification = array(
            'message' => 'Admin dengan No Induk '.$hapus->no_induk.' telah dihapus.',
            'alert-type' => 'error'
        );

        User::find($id)->delete();
        return redirect()->route('admin.akun.admin.index')->with($notification);
    }
}
