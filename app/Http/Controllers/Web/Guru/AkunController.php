<?php

namespace App\Http\Controllers\Web\Guru;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Guru;
class AkunController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:guru');
    }

    public function index(Request $request)
    {

        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }
        
        $nama = $request->cookie('nama');
        $guru = Guru::where('nama',$nama)->get()->first();

        return view('guru.akun.akun', compact('guru', 'no'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $akun = User::select('no_induk','id')->findOrFail($id);

        return view('guru.akun.edit_akun', compact('akun'));
    }

    public function update(Request $request, $id)
    {
        $akun = User::findOrFail($id);

        $password = $request->get('password');

        if ($password == '') {
            $data['password'] = $akun->password;
        } else {
            $data['password'] = bcrypt($request->get('password'));
        }

        $akun->update($data);

        $notification = array(
            'message' => 'Password Guru dengan No Induk '.$akun->no_induk.' telah diubah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.akun.guru.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
