<?php

namespace App\Http\Controllers\Web\Guru;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Buku;
use App\Siswa;

class BerandaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:guru');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');
        $page = $request->get('page');
        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }

        $jml_siswa = Siswa::where('tahun_ajaran', $tahun_ajaran)->where('hapus', '0')->count();

        $k_siswa = Siswa::where('tahun_ajaran', $tahun_ajaran)->where('hapus', '1')->count();
        $jml_k = $k_siswa;

        $buku = Buku::orderBy('tampil','desc')->limit(10)->get();

        return view('guru.beranda.beranda',compact('jml_siswa','jml_k','no','buku'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
