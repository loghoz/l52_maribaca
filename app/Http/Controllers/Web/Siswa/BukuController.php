<?php

namespace App\Http\Controllers\Web\Siswa;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Buku;
use App\MateriBuku;
use App\Nilai;
use App\Soal;
use App\siswa;
use App\MataPelajaran;

class BukuController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:siswa');
    }

    public function index(Request $request)
    {

        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 15 - 14;
        }else{
            $no=1;
        }

        $buku = Buku::where('judul_buku', 'LIKE', '%'.$q.'%')
            ->orderBy('id','desc')
            ->paginate(12);

        return view('siswa.buku.buku', compact('buku', 'no' ,'q'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request,$id)
    {
        $page = $request->get('page');
        if ($page=='') {
            $hal=0;
        }else{
            $hal= $page;
        }
        $buku = Buku::findOrFail($id);
        $jml  = MateriBuku::where('buku_id', $id)->count();
        $materi = MateriBuku::where('buku_id', $id)
                ->orderBy('materi','ASC')
                ->paginate(1);

        return view('siswa.buku.show_materi', compact('materi','buku','jml','hal'));
    }

    public function tambahview($id)
    {
        $buku = Buku::findOrFail($id);
        $dataBuku['tampil'] = $buku->tampil+1;
        $buku->update($dataBuku);
        
        return redirect('siswa/buku/'.$id);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
