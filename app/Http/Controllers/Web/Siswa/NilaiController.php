<?php

namespace App\Http\Controllers\Web\Siswa;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Nilai;
use App\Siswa;

class NilaiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:siswa');
    }

    public function index(Request $request)
    {
        $nama = $request->cookie('nama');
        $siswa = Siswa::where('nama', $nama)->get()->first();

        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 8 - 7;
        }else{
            $no=1;
        }

        $nilai = Nilai::where('siswa_id', $siswa->id)
            ->orderBy('id','desc')
            ->paginate(8);

        return view('siswa.nilai.nilai', compact('nilai', 'no' ,'q'));
    }
    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
