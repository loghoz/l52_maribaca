<?php

namespace App\Http\Controllers\Web\Siswa;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Soal;
use App\Buku;

class UjianController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $page = $request->get('page');
        $no = 1;
        if($page>1){
            $no = $page;
        }else{
            $no=1;
        }
        if ($page=='') {
            $hal=0;
        }else{
            $hal= $page;
        }
        $buku = Buku::findOrFail($id);
        $jml  = Soal::where('buku_id', $id)->count();
        $soal = Soal::where('buku_id', $id)
                ->orderBy('soal','ASC')
                ->paginate(1);

        return view('siswa.ujian.show_ujian', compact('soal','buku','hal','jml','no'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
