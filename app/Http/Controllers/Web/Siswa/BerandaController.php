<?php

namespace App\Http\Controllers\Web\Siswa;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Buku;
use App\Siswa;
class BerandaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('akses:siswa');
    }

    public function index(Request $request)
    {
        $q = $request->get('q');
        $r = $request->get('r');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 15 - 14;
        }else{
            $no=1;
        }

        $buku = Buku::where('judul_buku', 'LIKE', '%'.$q.'%')
            ->where('tingkat_kelas', 'LIKE', '%'.$r.'%')
            ->orderBy('id','desc')
            ->paginate(9);

        $populer = Buku::orderBy('tampil','desc')
            ->paginate(5);
        
        $kelas10 = Buku::where('tingkat_kelas','10')->count();
        $kelas11 = Buku::where('tingkat_kelas','11')->count();
        $kelas12 = Buku::where('tingkat_kelas','12')->count();
            
        return view('siswa.beranda.beranda', compact('buku','populer','kelas10','kelas11','kelas12', 'no' ,'q'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
