<?php

Route::auth();
Route::get('/', 'HomeController@index');

//cookie tahun
// Route::resource('tahun', 'Web\Admin\TahunAjaranController');
Route::get('tahun', 'Web\Admin\TahunAjaranController@coota');
Route::resource('home', 'HomeController');
Route::resource('admin/beranda', 'Web\Admin\BerandaProController');

// jurusan
Route::resource('admin/jurusan', 'Web\Admin\JurusanController');
Route::resource('admin/kelas', 'Web\Admin\KelasController');

//siswa
Route::resource('admin/siswa', 'Web\Admin\SiswaController');
Route::resource('admin/siswa/hapus', 'Web\Admin\SiswaController@hapus');
Route::resource('admin/siswa/balikin', 'Web\Admin\SiswaController@balikin');
Route::resource('admin/siswa/destroy', 'Web\Admin\SiswaController@destroy');

//guru
Route::resource('admin/guru', 'Web\Admin\GuruController');
Route::resource('admin/guru/hapus', 'Web\Admin\GuruController@hapus');
Route::resource('admin/guru/balikin', 'Web\Admin\GuruController@balikin');
Route::resource('admin/guru/destroy', 'Web\Admin\GuruController@destroy');

//akun
Route::resource('admin/akun/admin', 'Web\Admin\AkunController');
Route::resource('admin/akun/guru', 'Web\Admin\AkunGuruController');
Route::resource('admin/akun/siswa', 'Web\Admin\AkunSiswaController');

//mata pelajaran
Route::resource('admin/mapel/na', 'Web\Admin\MataPelajaranController');
Route::resource('admin/mapel/pro', 'Web\Admin\MapelProController');

//kotak sampah
Route::resource('admin/sampah', 'Web\Admin\KotakSampahController');

//buku
Route::resource('admin/buku', 'Web\Admin\BukuController');
Route::get('admin/buku/tambahview/{id}', 'Web\Admin\BukuController@tambahview');

//soal
Route::resource('admin/soal', 'Web\Admin\SoalController');

//nilai
Route::resource('admin/nilai', 'Web\Admin\NilaiController');

/////////////////////////////////////////////////////////////////////////////
// GURU
////////////////////////////////////////////////////////////////////////////
// beranda
Route::resource('guru/beranda', 'Web\Guru\BerandaController');

// siswa
Route::resource('guru/siswa', 'Web\Guru\SiswaController');

// akun
Route::resource('guru/akun', 'Web\Guru\AkunController');

// buku
Route::resource('guru/buku', 'Web\Guru\BukuController');
Route::get('guru/buku/tambahview/{id}', 'Web\Guru\BukuController@tambahview');

// nilai
Route::resource('guru/nilai', 'Web\Guru\NilaiController');

// pengaturan
Route::resource('guru/pengaturan', 'Web\Guru\PengaturanController');

//soal
Route::resource('guru/soal', 'Web\Guru\SoalController');

/////////////////////////////////////////////////////////////////////////////
// SISWA
////////////////////////////////////////////////////////////////////////////
// beranda
Route::resource('siswa/beranda', 'Web\Siswa\BerandaController');

// akun
Route::resource('siswa/akun', 'Web\Siswa\AkunController');

// buku
Route::resource('siswa/buku', 'Web\Siswa\BukuController');
Route::get('siswa/buku/tambahview/{id}', 'Web\Siswa\BukuController@tambahview');

// nilai
Route::resource('siswa/nilai', 'Web\Siswa\NilaiController');

// ujian
Route::resource('siswa/ujian', 'Web\Siswa\UjianController');

// pengaturan
Route::resource('siswa/pengaturan', 'Web\Siswa\PengaturanController');