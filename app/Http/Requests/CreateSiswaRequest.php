<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateSiswaRequest extends Request
{
    public function authorize()
  {
      return true;
  }

  public function rules()
   {
       switch ($this->method()) {
           case 'POST':
           {
               return [
                   'no_induk'  => 'required|numeric|unique:siswas',
                   'nama' => 'required|max:50|regex:/^[A-Za-z \t]*$/i',
                   'jk' => 'required',
                   'telp' => 'numeric',
                   'photo' => 'image|between:0,1024|mimes:jpeg,jpg,png'
               ];
           }

           case 'PUT':
           case 'PATCH':
           {
               return [
                'no_induk'  => 'required|numeric|unique:siswas',
                'nama' => 'required|max:50|regex:/^[A-Za-z \t]*$/i',
                'jk' => 'required',
                'telp' => 'numeric',
                'photo' => 'image|between:0,1024|mimes:jpeg,jpg,png'
               ];
           }

           default:break;
       }
   }

   public function messages()
   {
       return [

           'no_induk.required' => 'Tidak boleh kosong',
           'no_induk.unique' => 'NIS sudah ada',
           'no_induk.numeric' => 'Harus angka',

           'nama.required' => 'Tidak boleh kosong',
           'nama.max' => 'Maksimal 50 karakter',
           'nama.regex' => 'Tidak boleh terdapat angka dan simbol',

           'jk.required' => 'Pilih salah satu',
           
           'telp.numeric' => 'Harus angka',

           'file_photo.image' => 'File harus format .jpg .png .jpeg',
           'file_photo.mimes' => 'File harus format .jpg .png .jpeg',
           'file_photo.between' => 'Foto harus berukuran 100kb s.d 1Mb'
       ];
   }
}
