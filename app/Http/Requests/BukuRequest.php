<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BukuRequest extends Request
{
    public function authorize()
  {
      return true;
  }

  public function rules()
   {
       switch ($this->method()) {
           case 'POST':
           {
               return [
                   'judul_buku'         => 'required|max:100',
                   'tingkat_kelas'      => 'required',
                   'photo'              => 'image|between:0,1024|mimes:jpeg,jpg,png',
                   'buku'               => 'required|mimes:pdf|max:2048'
               ];
           }

           case 'PUT':
           case 'PATCH':
           {
               return [
                    'judul_buku'        => 'required|max:100',
                    'tingkat_kelas'     => 'required',
                    'photo'             => 'image|between:0,2048|mimes:jpeg,jpg,png',
                    'buku'              => 'mimes:pdf|max:2048'
               ];
           }

           default:break;
       }
   }

   public function messages()
   {
       return [

           'judul_buku.required' => 'Tidak boleh kosong',
           'judul_buku.max' => 'Maksimal 100 karakter',

           'tingkat_kelas.required' => 'Pilih salah satu',

           'photo.image' => 'File harus format .jpg .png .jpeg',
           'photo.mimes' => 'File harus format .jpg .png .jpeg',
           'photo.max' => 'Foto tidak boleh lebih dari 2Mb',

           'buku.required' => 'Tidak boleh kosong',
           'buku.mimes' => 'File harus format .pdf',
           'buku.max' => 'File pdf tidak boleh lebih dari 2Mb'
       ];
   }
}
