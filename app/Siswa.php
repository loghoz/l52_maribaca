<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswas';

    protected $fillable = [
        'no_induk','nama','jk','telp','photo','hapus',
    ];

    // relasi one to many ke nilai
    public function nilai()
    {
        return $this->hasMany('App\Nilai');
    }

    public function user(){
        return $this->hasOne('App\User','no_induk','no_induk');
    }

    //relasi one to many ke siswa
    public function pemkel()
    {
        return $this->hasMany('App\PembagianKelas');
    }
}
