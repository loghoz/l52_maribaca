<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriBuku extends Model
{
    protected $table = 'materi_bukus';

    protected $fillable = [
        'buku_id','materi','isi_materi',
    ];

    //relasi one to one ke buku
    public function buku()
    {
        return $this->belongsTo('App\Buku','buku_id');
    }
}
