<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $table = 'soals';

    protected $fillable = [
        'buku_id','soal','jawaban_a','jawaban_b','jawaban_c','jawaban_d','jawaban_benar',
        'photo_soal','photo_a','photo_b','photo_c','photo_d',
    ];

    //relasi one to one ke buku
    public function buku()
    {
        return $this->belongsTo('App\Buku','buku_id');
    }
}
