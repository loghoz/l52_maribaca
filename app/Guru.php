<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'gurus';

    protected $fillable = [
        'no_induk','nama','jk','telp','photo','tahun_ajaran','hapus',
    ];

    // relasi one to many ke buku
    public function buku()
    {
        return $this->hasMany('App\Buku');
    }

    public function user(){
        return $this->hasOne('App\User','no_induk','no_induk');
    }
}
