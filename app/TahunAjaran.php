<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    protected $table = 'tahun_ajarans';
    
    protected $fillable = [
        'tahun_ajaran',
    ];

    //relasi one to many ke siswa
    public function pemkel()
    {
        return $this->hasMany('App\PembagianKelas');
    }

    //relasi one to many ke siswa
    public function pempel()
    {
        return $this->hasMany('App\PembagianMapel');
    }
}
