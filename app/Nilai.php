<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    protected $table = 'nilais';

    protected $fillable = [
        'siswa_id','buku_id','nilai',
    ];

    //relasi one to one ke buku
    public function buku()
    {
        return $this->belongsTo('App\Buku','buku_id');
    }

    //relasi one to one ke siswa
    public function siswa()
    {
        return $this->belongsTo('App\Siswa','siswa_id');
    }
}
