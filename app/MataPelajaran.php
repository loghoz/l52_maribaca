<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model
{
    protected $table = 'mata_pelajarans';
    protected $with = ['jurusan'];
    protected $fillable = [
        'kode_mat_pel','mata_pelajaran','deskripsi',
    ];

    // relasi ke jurusan
    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan', 'jurusan_id');
    }
    
    // relasi one to many ke buku
    public function buku()
    {
        return $this->hasMany('App\Buku');
    }

    //relasi one to many ke siswa
    public function pempel()
    {
        return $this->hasMany('App\PembagianMapel');
    }
}
