@extends('layouts.user.app')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex text-center align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h2 class="text-white">
                        Pengaturan
                    </h2>
                </div>
            </div>
        </div>
    </section>

    <!--  Blog Area -->
    <section class="blog_area single-post-area p_120">
        <div class="container">
            <div class="row mt-80">

                <!-- CONTENT -->
                <div class="col-lg-12">
                    <div class="row">
                        
                        <div class="col-md-1"></div>
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <center><img class="author_img rounded-circle" src="{{ asset('itlabil/images/default/avatar1.png')}}" width="100px" alt=""></center><br>
                                </div>
                                <div class="col-md-4">
                                    Nama
                                </div>
                                <div class="col-md-8">
                                    : {{$siswa->nama}}
                                </div>
                                <div class="col-md-4">
                                    No induk
                                </div>
                                <div class="col-md-8">
                                    : {{$siswa->no_induk}}
                                </div>
                                <div class="col-md-4">
                                    Jenis Kelamin
                                </div>
                                <div class="col-md-8">
                                    @if($siswa->jk=='L')
                                        : Laki - Laki
                                    @else
                                        : Perempuan
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    Telpon
                                </div>
                                <div class="col-md-8">
                                    : {{$siswa->telp}}
                                </div>
                                <div class="col-md-12">
                                    Edit
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            Akun
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
@endsection
