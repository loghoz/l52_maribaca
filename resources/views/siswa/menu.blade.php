<div class="user-panel">
  <div class="pull-left image">
    <img src="{{ url('itlabil/images/default/')}}/{{ Cookie::get('photo') }}" class="img-circle" alt="User Image">
  </div>
  <div class="pull-left info">
    <p>{{ Cookie::get('nama') }}</p>
		<a href="{{ url('/tahun') }}">T.A. {{ Cookie::get('tahun_ajaran') }}</a> 
  </div>
</div>

<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MENU</li>

	{{--  BERANDA  --}}
	<li>
		<a href="{{ url('/siswa/beranda') }}">
			<i class="fa fa-home"></i>
			<span>Beranda</span>
		</a>
	</li>

	{{--  BUKU --}}
	<li>
		<a href="{{ url('/siswa/buku') }}">
			<i class="fa fa-book"></i>
			<span>Buku</span>
		</a>
	</li>

	{{--  NILAI --}}
	<li>
		<a href="{{ url('/siswa/nilai') }}">
			<i class="fa fa-edit"></i>
			<span>Nilai</span>
		</a>
	</li>

	{{--  AKUN  --}}
	<li><a href="{{ url('/siswa/akun') }}"><i class="fa fa-key"></i> <span>Akun</span></a></li>

	{{--  pengaturan  --}}
	<li>
		<a href="{{ url('/siswa/pengaturan') }}">
			<i class="fa fa-cog"></i>
			<span>Pengaturan</span>
		</a>
	</li>

</ul>