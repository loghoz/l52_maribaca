@extends('layouts.user.app')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex text-center align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h2 class="text-white">
                        Daftar Nilai
                    </h2>
                </div>
            </div>
        </div>
    </section>

    <!--  Blog Area -->
    <section class="blog_area single-post-area p_120">
        <div class="container">
            <div class="row mt-80">

                <!-- CONTENT -->
                <div class="col-lg-12">
                    <div class="row">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Buku</th>
                                    <th>Nilai</th>
                                    <th>Tanggal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($nilai as $item)
                                    <tr>
                                        <td width="20px">{{$no++}}</td>
                                        <td>{{$item->buku->judul_buku}}</td>
                                        <td>{{$item->nilai}}</td>
                                        <td>{{ $item->created_at->format('h:m') }} - {{ $item->created_at->format('d M Y') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $nilai->appends(compact('q', 'page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
@endsection
