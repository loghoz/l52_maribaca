@extends('layouts.user.app')

@section('content')

    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex text-center align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <p class="text-white link-nav">
                        Selamat Datang di Aplikasi
                    </p>
                    <h1 class="text-white">
                        Mari Baca
                    </h1>
                </div>
            </div>
        </div>
    </section>

    <!--  Blog Area -->
    <section class="blog_area single-post-area p_120">
        <div class="container">
            <div class="row mt-80">

                <!-- CONTENT -->
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <!-- BUKU -->
                            <div class="row">
                                @forelse ($buku as $item)
                                    <div class="col-md-4">
                                        <div class="box border-itlabil">
                                            <div class="box-body box-profile">
                                                <img class="img-cover img-responsive" src="{{url('itlabil/images/cover')}}/{{ $item->photo }}" alt="User profile picture">
                                                <a href="{{ url('/siswa/buku/').'/'.$item->id }}"><h4 class="text-judul text-center" title="{{$item->judul_buku}}">{{$item->judul_buku}}</h3></a>
                                                <p class="text-center">
                                                    <small class="label bg-red"><i class="fa fa-fw fa-user"></i>{{$item->guru->nama}}</small><br>
                                                    <small class="label bg-red"><i class="fa fa-fw fa-black-tie"></i>{{$item->tingkat_kelas}}</small>
                                                    <small class="label bg-yellow"><i class="fa fa-fw fa-eye"></i>{{$item->tampil}}</small><br>
                                                    <small class="label bg-blue"><i class="fa fa-fw fa-book"></i>{{$item->mapel->mata_pelajaran}}</small>
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                @empty
                                    <div class="col-md-12 text-center">
                                        @if (isset($q))
                                            <h1>:(</h1>
                                            <p>buku tidak ditemukan.</p>
                                        @endif
                                        <br><p><a href="{{ url('/siswa/beranda') }}">Lihat semua buku <i class="fa fa-arrow-right"></i></a></p>
                                    </div>
                                @endforelse
                            </div>
                            {{ $buku->appends(compact('q', 'page'))->links() }}
                        </div>
                    </div>
                </div>

                <!-- SIDEMENU -->
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">

                        <!-- SEARCH -->
                        <aside class="single_sidebar_widget search_widget">
                            <form method="GET" action="{{ url('siswa/beranda') }}">
                                <div class="input-group">
                                        <input name="q" type="text" class="form-control" placeholder="Cari Buku">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default">
                                                <i class="lnr lnr-magnifier"></i>
                                            </button>
                                        </span>
                                    
                                </div>
                            </form>
                            <div class="br"></div>
                        </aside>

                        <!-- PROFIL -->
                        <aside class="single_sidebar_widget author_widget">
                            <img class="author_img rounded-circle" src="{{ asset('itlabil/images/default/avatar1.png')}}" width="100px" alt="">
                            <h4>{{ Cookie::get('nama') }}</h4>
                            <p>10 TKJ 1</p>
                            <div class="br"></div>
                        </aside>

                        <!-- BUKU POPULER -->
                        <aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title">Buku Populer</h3>
                            @forelse ($populer as $item)
                                <div class="media post_item">
                                    <div class="media-body">
                                        <a href="{{ url('/siswa/buku/').'/'.$item->id }}">
                                            <h3>{{ $item->judul_buku}}</h3>
                                        </a>
                                        <p>{{ $item->tampil}}</p>
                                    </div>
                                </div>
                            @empty
                                <div class="media post_item">
                                    <div class="media-body">
                                        <h3>Tidak Ada Buku Populer</h3>
                                    </div>
                                </div>
                            @endforelse
                            
                            <div class="br"></div>
                        </aside>

                        <!-- CATEGORY BUKU -->
                        <aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title">Kategori Buku</h4>
                            <ul class="list cat-list">
                                <p><b>Kelas</b></p>
                                <li>
                                    <a href="{{ url('/siswa/beranda?r=10') }}" class="d-flex justify-content-between">
                                        <p>Kelas 10</p>
                                        <p>{{$kelas10}}</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/siswa/beranda?r=11') }}" class="d-flex justify-content-between">
                                        <p>Kelas 11</p>
                                        <p>{{$kelas11}}</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/siswa/beranda?r=12') }}" class="d-flex justify-content-between">
                                        <p>Kelas 12</p>
                                        <p>{{$kelas12}}</p>
                                    </a>
                                </li>

                                <br>
                                <!-- <p><b>Jenis Mapel</b></p>
                                <li>
                                    <a href="#" class="d-flex justify-content-between">
                                        <p>Normatif Adaptif</p>
                                        <p>29</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="d-flex justify-content-between">
                                        <p>Produktif</p>
                                        <p>15</p>
                                    </a>
                                </li> -->
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>

@endsection
