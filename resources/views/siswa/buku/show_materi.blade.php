@extends('layouts.user.app')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex text-center align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h2 class="text-white">
                        {{$buku->judul_buku}}
                    </h2>
                    <p class="text-white link-nav">
                        oleh {{$buku->guru->nama}}
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!--  Blog Area -->
    <section class="blog_area single-post-area p_120">
        <div class="container">
            <div class="row mt-80">

                <!-- CONTENT -->
                <div class="col-lg-12">
                    <div class="row">
                        @foreach ($materi as $item)
                            <div class="col-lg-12 col-md-12">
                                <h2>{{$item->materi}}</h2>
                                <br>
                                <p align="justify">
                                    {{$item->isi_materi}}
                                </p>
                                <br>
                            </div>
                        @endforeach
                        <div class="col-lg-6">{{ $materi->appends(compact('q', 'page'))->links() }}</div>
                        <div class="col-lg-6" align="right">
                            @if($jml==$hal)
                                <a href="{{ url('/siswa/ujian/').'/'.$item->buku_id }}" class="btn btn-primary">Ujian</a>
                            @else
                                <button type="button" class="btn btn-primary" disabled>Ujian</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
@endsection
