@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Akun
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Akun</h3>
                    </div>

                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td align="center">No Induk</td>
                                <td align="center">Password</td>
                                <td colspan="2" align="center">Aksi</td>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center">{{ $siswa->no_induk }}</td>
                                    <td align="center">*****</td>
                                    <td align="center">
                                        <a href="{{ route('siswa.akun.edit', $siswa->user->id) }}" class="btn btn-primary">Ubah</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
