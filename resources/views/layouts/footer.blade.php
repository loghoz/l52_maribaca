<div class="pull-right hidden-xs">
    <b>Version</b> 1.0
</div>
<strong>Copyright &copy; 2018 <a data-toggle="modal" data-target="#detail">iTLabil</a></strong>

<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div align="center">
                  <h4 class="modal-title" id="header">
                      iTLabil
                  </h4>
                </div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <tbody>
                              <tr>
                                <th></th>
                                <td><img class="img-circle" src="{{ url('itlabil/images/default/admin-1.jpg') }}" width="100" height="100" class="img-circle" alt="User Image"></td>
                                <td><img class="img-circle" src="{{ url('itlabil/images/default/admin-1.jpg') }}" width="100" height="100" class="img-circle" alt="User Image"></td>
                                <td><img class="img-circle" src="{{ url('itlabil/images/default/admin-1.jpg') }}" width="100" height="100" class="img-circle" alt="User Image"></td>
                              </tr>
                              <tr>
                                <th>Nama</th>
                                <td>Erwin Dianto</td>
                                <td>Taufik Nurhuda</td>
                                <td>Dinar Maharani Deffa</td>
                              </tr>
                              <tr>
                                <th>NPM</th>
                                <td>15312426</td>
                                <td>15312444</td>
                                <td>15312695</td>
                              </tr>
                              <tr>
                                <th>NPM</th>
                                <td>Programer</td>
                                <td>Adminitrator Server</td>
                                <td>Designer</td>
                              </tr>
                              <tr>
                                <th>Telp</th>
                                <td>0896-3107-3926</td>
                                <td>0896-3107-3926</td>
                                <td>0896-3107-3926</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </div>
    </div>
</div>
