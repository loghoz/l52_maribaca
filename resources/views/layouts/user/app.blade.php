<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset ('itlabil/images/default/logo.png') }}">
    <meta name="author" content="CodePixar">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="UTF-8">
    <title>Mari Baca</title>

    <link href="{{ asset('itlabil/admin/dist/css/custom.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('itlabil/user/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('itlabil/user/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('itlabil/user/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('itlabil/user/css/ion.rangeSlider.css') }}" />
    <link rel="stylesheet" href="{{ asset('itlabil/user/css/ion.rangeSlider.skinFlat.css') }}" />
    <link rel="stylesheet" href="{{ asset('itlabil/user/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('itlabil/user/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('itlabil/user/css/main.css') }}">
</head>

<body>

    <!-- Start Header Area -->
    <header class="default-header">
        <div class="main-menu">
            <div class="container">
                <div class="row align-items-center justify-content-between d-flex">
                    <div id="logo">
                        <a href="{{ asset('/') }}"><img src="{{ asset('itlabil/images/default/maribaca.png') }}" alt="" title="" /></a>
                    </div>
                    <nav id="nav-menu-container">
                        <ul class="nav-menu">
                            <li><a href="{{ asset('/') }}">Beranda</a></li>
                            <li><a href="{{ asset('siswa/nilai') }}">Nilai</a></li>
                            <li><a href="{{ asset('/logout') }}">Keluar</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    @yield('content')

    <!-- start footer Area -->
    <footer class="footer-area">
        <div class="container">
            <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
                <p class="footer-itlabil">
                    Copyright &copy;2018 Design By iTLabil
                </p>
            </div>
        </div>
    </footer>

    <!-- SCRIPT -->
    <script src="{{ asset('itlabil/user/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/jquery.sticky.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/ion.rangeSlider.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/jquery-ui.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('itlabil/user/js/main.js')}}"></script>
</body>

</html>