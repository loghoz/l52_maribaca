<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Login</title>

        <!-- Styles -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
       
        <!-- CSS -->

        <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/morris.js/morris.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <link href="{{ asset('itlabil/user/toast/toastr.min.css') }}" rel="stylesheet">

    <style>
        .bg {
            font-family: 'Lato';
            background-image: url('{{ asset ('itlabil/images/default/bg-login.jpg') }}');
            background-repeat: no-repeat;
            position: fixed;
            width: 100%;
            height: 100%;
            background-size: 100%;
        }

        .shadow{
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);;
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>

<body class="bg hold-transition login-page">
  <div class="login-box">
  <div class="login-logo">
    <div align="center" style="padding-top:10px;">
        <img src="{{asset('itlabil/images/default/logo.png')}}" width="150px" height="150px" alt="logo pelita" />
    </div>
    <font color="#ffffff"><b>Mari</b>Baca</font>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body shadow">
    <p class="login-box-msg">LOGIN</p>

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('no_induk') ? ' has-error' : '' }}">
            <label for="no_induk" class="col-md-4 control-label">No Induk</label>

            <div class="col-md-8 has-feedback">
                <input id="no_induk" type="no_induk" class="form-control" name="no_induk" value="{{ old('no_induk') }}">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @if ($errors->has('no_induk'))
                    <span class="help-block">
                        <strong>{{ $errors->first('no_induk') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Password</label>

            <div class="col-md-8 has-feedback">
                <input id="password" type="password" class="form-control" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group" align="right">
            <div class="col-md-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Login
                </button>
            </div>
        </div>
    </form>
  </div>
</div>

<!-- Scripts -->
<!-- JS -->
<script src="{{ asset('itlabil/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/morris.js/morris.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/adminlte.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/pages/dashboard.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/demo.js') }}"></script>

        <script type="text/javascript" src="{{ asset('itlabil/user/toast/toastr.min.js') }}"></script>

        <script>
            @if(Session::has('message'))
                var type="{{Session::get('alert-type','info')}}"

                switch(type){
                    case 'info':
                        toastr.info("{{ Session::get('message') }}");
                        break;
                    case 'success':
                        toastr.success("{{ Session::get('message') }}");
                        break;
                    case 'warning':
                        toastr.warning("{{ Session::get('message') }}");
                        break;
                    case 'error':
                        toastr.error("{{ Session::get('message') }}");
                        break;
                }
            @endif
        </script>

</body>
</html>
