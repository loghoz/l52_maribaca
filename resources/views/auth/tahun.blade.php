<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SMK Pelita</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
       
       <!-- CSS -->

       <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/bower_components/morris.js/morris.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
       <link href="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

       <link href="{{ asset('itlabil/user/toast/toastr.min.css') }}" rel="stylesheet">

   <style>
       .bg {
           font-family: 'Lato';
           background-image: url('{{ asset ('itlabil/images/default/bg-login.jpg') }}');
           background-repeat: no-repeat;
           position: fixed;
           width: 100%;
           height: 100%;
           background-size: 100%;
       }

       .shadow{
         box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);;
       }

       .fa-btn {
           margin-right: 6px;
       }
   </style>
</head>

<body class="bg hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <div align="center">
          <img src="{{asset('itlabil/images/default/logo.png')}}" width="150px" height="150px" alt="logo pelita" />
      </div>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body shadow">
      <p class="login-box-msg">Pilih Tahun Ajaran</p>
      {!! Form::open(['route' => 'tahun.store','class'=>'form-horizontal'])!!}
            <div class="form-group{{ $errors->has('tahun_ajaran') ? ' has-error' : '' }}">
                {!! Form::select('tahun_ajaran',$ta ,null,array('class'=>'form-control has-feedback')) !!}
                <small class="text-danger">{{ $errors->first('tahun_ajaran') }}</small>
            </div>

            <div class="form-group" align="right">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> Pilih
                    </button>
                </div>
            </div>
            @if(session()->has('message'))
                <div class="alert alert-danger">
                    {{ session()->get('message') }}
                </div>
            @endif
        {!! Form::close() !!}
    </div>
  </div>

        <!-- Scripts -->
        <!-- JS -->
        <script src="{{ asset('itlabil/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/morris.js/morris.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/adminlte.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/pages/dashboard.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/demo.js') }}"></script>

        <script type="text/javascript" src="{{ asset('itlabil/user/toast/toastr.min.js') }}"></script>

        <script>
            @if(Session::has('message'))
                var type="{{Session::get('alert-type','info')}}"

                switch(type){
                    case 'info':
                        toastr.info("{{ Session::get('message') }}");
                        break;
                    case 'success':
                        toastr.success("{{ Session::get('message') }}");
                        break;
                    case 'warning':
                        toastr.warning("{{ Session::get('message') }}");
                        break;
                    case 'error':
                        toastr.error("{{ Session::get('message') }}");
                        break;
                }
            @endif
        </script>

</body>
</html>