<div class="col-md-12">
    <div class="col-md-8">
        <!-- SOAL -->
        <div class="form-group{{ $errors->has('soal') ? ' has-error' : '' }}">
            {!! Form::label('soal', 'Soal', ['class'=>'control-label col-sm-2']) !!}
            <div class="col-sm-10">
                <div class="box-body pad">
                    {!! Form::textarea('soal', null, ['class' => 'textarea','placeholder'=>'Ketik Soal','style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <small class="text-danger">{{ $errors->first('soal') }}</small>
            </div>
        </div>

        <!-- Jawaban A -->
        <div class="form-group{{ $errors->has('jawaban_a') ? ' has-error' : '' }}">
            {!! Form::label('jawaban_a', 'Jawaban A', ['class'=>'control-label col-sm-2']) !!}
            <div class="col-sm-10">
                <div class="box-body pad">
                    {!! Form::textarea('jawaban_a', null, ['class' => 'textarea','placeholder'=>'Ketik Jawaban','style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <small class="text-danger">{{ $errors->first('jawaban_a') }}</small>
            </div>
        </div>

        <!-- Jawaban B -->
        <div class="form-group{{ $errors->has('jawaban_b') ? ' has-error' : '' }}">
            {!! Form::label('jawaban_b', 'Jawaban B', ['class'=>'control-label col-sm-2']) !!}
            <div class="col-sm-10">
                <div class="box-body pad">
                    {!! Form::textarea('jawaban_b', null, ['class' => 'textarea','placeholder'=>'Ketik Jawaban','style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <small class="text-danger">{{ $errors->first('jawaban_b') }}</small>
            </div>
        </div>

        <!-- Jawaban C -->
        <div class="form-group{{ $errors->has('jawaban_c') ? ' has-error' : '' }}">
            {!! Form::label('jawaban_c', 'Jawaban C', ['class'=>'control-label col-sm-2']) !!}
            <div class="col-sm-10">
                <div class="box-body pad">
                    {!! Form::textarea('jawaban_c', null, ['class' => 'textarea','placeholder'=>'Ketik Jawaban','style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <small class="text-danger">{{ $errors->first('jawaban_c') }}</small>
            </div>
        </div>

        <!-- Jawaban D -->
        <div class="form-group{{ $errors->has('jawaban_d') ? ' has-error' : '' }}">
            {!! Form::label('jawaban_d', 'Jawaban D', ['class'=>'control-label col-sm-2']) !!}
            <div class="col-sm-10">
                <div class="box-body pad">
                    {!! Form::textarea('jawaban_d', null, ['class' => 'textarea','placeholder'=>'Ketik Jawaban','style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <small class="text-danger">{{ $errors->first('jawaban_d') }}</small>
            </div>
        </div>

    </div>
    <div class="col-md-4">
        <!-- PHOTO SOAL -->
        <div class="form-group{{ $errors->has('photo_soal') ? ' has-error' : '' }}">
            {!! Form::label('photo_soal', 'Foto Soal', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::file('photo_soal') !!}
            </div>
        </div>

        <hr>
        
        <!-- PHOTO JAWABAN A -->
        <div class="form-group{{ $errors->has('photo_a') ? ' has-error' : '' }}">
            {!! Form::label('photo_a', 'Foto Jawaban A', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::file('photo_a') !!}
            </div>
        </div>
        
        <hr>

        <!-- PHOTO JAWABAN B -->
        <div class="form-group{{ $errors->has('photo_b') ? ' has-error' : '' }}">
            {!! Form::label('photo_b', 'Foto Jawaban B', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::file('photo_b') !!}
            </div>
        </div>
        
        <hr>

        <!-- PHOTO JAWABAN C -->
        <div class="form-group{{ $errors->has('photo_c') ? ' has-error' : '' }}">
            {!! Form::label('photo_c', 'Foto Jawaban C', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::file('photo_c') !!}
            </div>
        </div>
        
        <hr>
        
        <!-- PHOTO JAWABAN D -->
        <div class="form-group{{ $errors->has('photo_d') ? ' has-error' : '' }}">
            {!! Form::label('photo_d', 'Foto Jawaban D', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::file('photo_d') !!}
            </div>
        </div>

        <hr>

        <!-- JAWABAN -->
        <div class="form-group{{ $errors->has('jawaban_benar') ? ' has-error' : '' }}">
            {!! Form::label('jawaban_benar', 'Jawaban Benar', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                <select class="form-control" name="jawaban_benar" id="">
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
    </div>
</div>
