<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Data guru</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">

            {{--  NAMA  --}}
            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                {!! Form::label('nama', '* Nama Lengkap', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                </div>
            </div>

            {{--  JK  --}}
            <div class="form-group{{ $errors->has('jk') ? ' has-error' : '' }}">
                {!! Form::label('jk', '* Jenis Kelamin', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-4">
                    <label class="radio-inline">
                        @if ($guru->jk == 'L')
                            {!! Form::radio('jk', 'L',true) !!} Laki - Laki
                        @else
                            {!! Form::radio('jk', 'L') !!} Laki - Laki
                        @endif
                    </label>
                </div>
                <div class="col-sm-4">
                    <label class="radio-inline">
                        @if ($guru->jk == 'P')
                            {!! Form::radio('jk', 'P',true) !!} Perempuan
                        @else
                            {!! Form::radio('jk', 'P') !!} Perempuan
                        @endif
                    </label>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('jk') }}</small>
                </div>
            </div>

            {{--  NIS  --}}
            <div class="form-group{{ $errors->has('no_induk') ? ' has-error' : '' }}">
                {!! Form::label('no_induk', '* NIG', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('no_induk', ( isset($guru->no_induk) ? $guru->no_induk : null ), ['class' => 'form-control','placeholder'=>'No Induk Guru']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('no_induk') }}</small>
                </div>
            </div>

            {{--  TELP  --}}
            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                {!! Form::label('telp', 'Telp / HP', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                {!! Form::text('telp', null, ['class' => 'form-control','placeholder'=>'Telp / HP']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                <small class="text-danger">{{ $errors->first('telp') }}</small>
                </div>
            </div>

            {{--  email  --}}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                {!! Form::email('email', $guru->user->email, ['class' => 'form-control','placeholder'=>'Email']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                <small class="text-danger">{{ $errors->first('email') }}</small>
                </div>
            </div>

            {{--  PHOTO  --}}
            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                {!! Form::label('photo', 'Foto', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::file('photo') !!}
                </div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('photo') }}</small>
                    <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB <br>dan Resolusi Gambar Square ex: 512 x 512</p>
                </div>
                @if ($guru->photo == 'avatar1.png')
                @elseif ($guru->photo == 'avatar3.png')
                @else()
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <br><p>Foto Sebelumnya:</p>
                            <div class="thumbnail">
                                <img src="{{ url('itlabil/images/guru/' . $guru->photo) }}" class="img-rounded">
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <input type="hidden" name="tahun_ajaran" value="{{ Cookie::get('tahun_ajaran') }}">
            <input type="hidden" name="hapus" value="0">
        </div>
    </div>
</div>
<div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>
