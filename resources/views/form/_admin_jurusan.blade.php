<div class="col-sm-6">
    <div class="form-group{{ $errors->has('bidang_keahlian') ? ' has-error' : '' }}">
        {!! Form::label('bidang_keahlian', 'Bidang Keahlian', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('bidang_keahlian', null, ['class' => 'form-control','placeholder'=>'Bidang Keahlian']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('bidang_keahlian') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('jurusan') ? ' has-error' : '' }}">
        {!! Form::label('jurusan', 'Jurusan', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('jurusan', null, ['class' => 'form-control','placeholder'=>'Jurusan']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('jurusan') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('kode_jurusan') ? ' has-error' : '' }}">
        {!! Form::label('kode_jurusan', 'Kode Jurusan', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('kode_jurusan', null, ['class' => 'form-control','placeholder'=>'Contoh : AK ( Akuntansi )']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('kode_jurusan') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
