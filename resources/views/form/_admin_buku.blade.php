{{--  DATA SISWA  --}}
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Form Data Buku</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">

            {{--  GURU  --}}
            <div class="form-group{{ $errors->has('guru_id') ? ' has-error' : '' }}">
                {!! Form::label('guru_id', 'Guru', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('guru_id',$guru ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                <small class="text-danger">{{ $errors->first('guru_id') }}</small>
                </div>
            </div>

            {{--  Mata Pelajaran  --}}
            <div class="form-group{{ $errors->has('mata_pelajaran_id') ? ' has-error' : '' }}">
                {!! Form::label('mata_pelajaran_id', 'Mata Pelajaran', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('mata_pelajaran_id',$mapel ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                <small class="text-danger">{{ $errors->first('mata_pelajaran_id') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('tingkat_kelas') ? ' has-error' : '' }}">
                {!! Form::label('tingkat_kelas', 'Tingkat', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-3">
                    <label class="radio-inline">
                        {!! Form::radio('tingkat_kelas', '10') !!} 10
                    </label>
                </div>
                <div class="col-sm-3">
                    <label class="radio-inline">
                        {!! Form::radio('tingkat_kelas', '11') !!} 11
                    </label>
                </div>
                <div class="col-sm-2">
                    <label class="radio-inline">
                        {!! Form::radio('tingkat_kelas', '12') !!} 12
                    </label>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                <small class="text-danger">{{ $errors->first('tingkat_kelas') }}</small>
                </div>
            </div>

            {{--  judul_buku  --}}
            <div class="form-group{{ $errors->has('judul_buku') ? ' has-error' : '' }}">
                {!! Form::label('judul_buku', '* Judul Buku', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('judul_buku', null, ['class' => 'form-control','placeholder'=>'Judul Buku']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('judul_buku') }}</small>
                </div>
            </div>

            {{--  PHOTO  --}}
            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                {!! Form::label('photo', 'Cover', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::file('photo') !!}
                </div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('photo') }}</small>
                    <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB <br>
                    dan Resolusi Gambar Square ex: 724 x 1024</p>
                </div>
            </div>

            {{--  BUKU  --}}
            <div class="form-group{{ $errors->has('buku') ? ' has-error' : '' }}">
                {!! Form::label('buku', 'Buku', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::file('buku') !!}
                </div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('buku') }}</small>
                    <p>Pastikan Ukuran File Tidak Lebih dari 5 MB <br>dan format buku berupa PDF</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>
