@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Kotak Sampah
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Siswa</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/sampah', 'method'=>'get', 'class'=>'form-inline'])!!}
                            <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                                {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Nama Siswa ....']) !!}
                                {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                            </div>
                            {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/sampah', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <input type="hidden" name="q" value="">
                                {!! Form::submit('Semua Data Siswa', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-12">
                        @forelse ($siswa as $item)
                            @if ($item->jk=='L')
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-red"><img src="../itlabil/images/siswa/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                            <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}} <i class="fa fa-fw fa-home"></i>{{$item->kelas->kode_kelas}}</span>
                                            <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                            <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                            <div align="right">
                                                <a href="{{ url('admin/siswa/balikin', $item->id) }}"><i class="fa fa-fw fa-undo"></i></a>
                                                <a href="{{ url('admin/siswa/destroy', $item->id) }}" class="js-submit-confirm"><i class="fa fa-fw fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-aqua"><img src="../itlabil/images/siswa/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                            <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}} <i class="fa fa-fw fa-home"></i>{{$item->kelas->kode_kelas}}</span>
                                            <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                            <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                            <div align="right">
                                                <a href="{{ url('admin/siswa/balikin', $item->id) }}"><i class="fa fa-fw fa-undo"></i></a>
                                                <a href="{{ url('admin/siswa/destroy', $item->id) }}" class="js-submit-confirm"><i class="fa fa-fw fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @empty
                        <div class="col-md-12 text-center">
                            @if (isset($q))
                            <h1>:(</h1>
                            <p>Siswa tidak ditemukan.</p>
                            @endif
                            <p><a href="{{ url('/admin/sampah') }}">Lihat semua data siswa <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                        @endforelse
                        {{ $siswa->appends(compact('page'))->links() }}
                    </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Guru</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/sampah', 'method'=>'get', 'class'=>'form-inline'])!!}
                            <div class="form-group {!! $errors->has('r') ? 'has-error' : '' !!}">
                                {!! Form::text('r', isset($r) ? $r : null, ['class'=>'form-control', 'placeholder' => 'Nama Guru ....']) !!}
                                {!! $errors->first('r', '<p class="help-block">:message</p>') !!}
                            </div>
                            {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/sampah', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <input type="hidden" name="r" value="">
                                {!! Form::submit('Semua Data Guru', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-12">
                        @forelse ($guru as $item)
                            @if ($item->jk=='L')
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-red"><img src="../itlabil/images/guru/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                            <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}}</span>
                                            <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                            <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                            <div align="right">
                                                <a href="{{ url('admin/guru/balikin', $item->id) }}"><i class="fa fa-fw fa-undo"></i></a>
                                                <a href="{{ url('admin/guru/destroy', $item->id) }}"><i class="fa fa-fw fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-aqua"><img src="../itlabil/images/guru/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                            <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}}</span>
                                            <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                            <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                            <div align="right">
                                                <a href="{{ url('admin/guru/balikin', $item->id) }}"><i class="fa fa-fw fa-undo"></i></a>
                                                <a href="{{ url('admin/guru/destroy', $item->id) }}"><i class="fa fa-fw fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @empty
                        <div class="col-md-12 text-center">
                            @if (isset($q))
                            <h1>:(</h1>
                            <p>Data Guru tidak ditemukan.</p>
                            @endif
                            <p><a href="{{ url('/admin/sampah') }}">Lihat semua data guru <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                        @endforelse
                        {{ $guru->appends(compact('page'))->links() }}
                    </div>
        </div>
    </section>
@endsection
