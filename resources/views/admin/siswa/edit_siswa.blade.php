@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Siswa
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
                <a href="{{ url('/admin/siswa') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Data Siswa</a>
                <br><br>
            </div>
            <div class="col-md-12">
                {!! Form::model($siswa, ['route' => ['admin.siswa.update', $siswa],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                    @include('form._admin_edit_siswa', ['model' => $siswa])
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
