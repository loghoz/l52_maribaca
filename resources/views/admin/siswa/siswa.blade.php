@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Siswa
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Siswa</h3>
                    </div>
                    <div class="box-body">
                      <div class="col-md-4">
                        {!! Form::open(['url' => 'admin/siswa', 'method'=>'get', 'class'=>'form-inline'])!!}
                          <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                              {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Nama Siswa ....']) !!}
                              {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                          </div>
                          {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                      </div>
                      <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/siswa', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <input type="hidden" name="q" value="">
                                {!! Form::submit('Semua Data Siswa', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                          </div>
                      <div class="col-md-4" align="right">
                        <a href="{{ route('admin.siswa.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Siswa</a>
                        
                        {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#upload"><i class="fa fa-upload"></i> Upload</button>

                        <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" align="left">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <center><h4 class="modal-title" id="upload">
                                            Upload Data Siswa
                                        </h4></center>
                                    </div>

                                    <div class="modal-body">
                                      {!! Form::open(['url' => 'admin/siswa/upload', 'method'=>'post','enctype'=>'multipart/form-data', 'class'=>'form-horizontal','files'=>true])!!}
                                        <div class="form-group{{ $errors->has('upload_siswa') ? ' has-error' : '' }}">
                                            {!! Form::label('upload_siswa', 'Pilih File', ['class'=>'control-label col-sm-4']) !!}
                                            <div class="col-sm-8">
                                                {!! Form::file('upload_siswa') !!}
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </div>
                                            <div class="col-sm-8">
                                                <small class="text-danger">{{ $errors->first('upload_siswa') }}</small>
                                                *Pastikan extensi file .csv
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        {{ Form::button('<i class="fa fa-upload"></i> Upload', ['value'=>'import','type'=>'submit','class' => 'btn btn-primary']) }}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div> --}}

                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($siswa as $item)
                @if ($item->jk=='L')
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><img src="../itlabil/images/siswa/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                            <div class="info-box-content">
                                <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}}
                                <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                <div align="right">
                                    <a href="{{ route('admin.siswa.edit', $item->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                                    <a href="{{ url('admin/siswa/hapus', $item->id) }}"><i class="fa fa-fw fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><img src="../itlabil/images/siswa/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                            <div class="info-box-content">
                                <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}}
                                <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                <div align="right">
                                    <a href="{{ route('admin.siswa.edit', $item->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                                    <a href="{{ url('admin/siswa/hapus', $item->id) }}"><i class="fa fa-fw fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
              @empty
              <div class="col-md-12 text-center">
                @if (isset($q))
                  <h1>:(</h1>
                  <p>Siswa tidak ditemukan.</p>
                @endif
                  <p><a href="{{ url('/admin/siswa') }}">Lihat semua data siswa <i class="fa fa-arrow-right"></i></a></p>
              </div>
            @endforelse
        </div>
        {{ $siswa->appends(compact('q', 'page'))->links() }}
    </section>
@endsection
