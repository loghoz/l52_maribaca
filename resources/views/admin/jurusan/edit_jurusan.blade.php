@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Jurusan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Jurusan</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($jurusan, ['route' => ['admin.jurusan.update', $jurusan],'method' =>'patch','class'=>'form-horizontal'])!!}
                        @include('form._admin_jurusan', ['model' => $jurusan])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
