@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Jurusan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Jurusan</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.jurusan.store', 'class'=>'form-horizontal'])!!}
                            @include('form._admin_jurusan')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Jurusan</h3>
                    </div>
                    <div class="box-body">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Kode Jurusan</td>
                                <td>Program Jurusan</td>
                                <td>Bidang Keahlian</td>
                                <td colspan="2" align="center">Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($jurusan as $jurus)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $jurus->kode_jurusan }}</td>
                                    <td>{{ $jurus->jurusan }}</td>
                                    <td>{{ $jurus->bidang_keahlian }}</td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.jurusan.edit', $jurus->id) }}" class="btn btn-primary">Ubah</a>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::model($jurus, ['route' => ['admin.jurusan.destroy', $jurus], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                  {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $jurusan->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
