@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Guru
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Guru</h3>
                    </div>
                    <div class="box-body">
                      <div class="col-md-4">
                        {!! Form::open(['url' => 'admin/guru', 'method'=>'get', 'class'=>'form-inline'])!!}
                          <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                              {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Nama Guru ....']) !!}
                              {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                          </div>
                          {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                      </div>
                      <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/guru', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <input type="hidden" name="q" value="">
                                {!! Form::submit('Semua Data Guru', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                          </div>
                      <div class="col-md-4" align="right">
                        <a href="{{ route('admin.guru.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Guru</a>

                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($guru as $item)
                @if ($item->jk=='L')
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><img src="../itlabil/images/guru/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                            <div class="info-box-content">
                                <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}}</span>
                                <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                <div align="right">
                                    <a href="{{ route('admin.guru.edit', $item->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                                    <a href="{{ url('admin/guru/hapus', $item->id) }}"><i class="fa fa-fw fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><img src="../itlabil/images/guru/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                            <div class="info-box-content">
                                <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}}</span>
                                <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                <div align="right">
                                    <a href="{{ route('admin.guru.edit', $item->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                                    <a href="{{ url('admin/guru/hapus', $item->id) }}"><i class="fa fa-fw fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
              @empty
              <div class="col-md-12 text-center">
                @if (isset($q))
                  <h1>:(</h1>
                  <p>guru tidak ditemukan.</p>
                @endif
                  <p><a href="{{ url('/admin/guru') }}">Lihat semua data guru <i class="fa fa-arrow-right"></i></a></p>
              </div>
            @endforelse
        </div>
        {{ $guru->appends(compact('q', 'page'))->links() }}
    </section>
@endsection
