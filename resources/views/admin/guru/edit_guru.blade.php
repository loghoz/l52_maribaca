@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Guru
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
                <a href="{{ url('/admin/guru') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Data guru</a>
                <br><br>
            </div>
            <div class="col-md-12">
                {!! Form::model($guru, ['route' => ['admin.guru.update', $guru],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                    @include('form._admin_edit_guru', ['model' => $guru])
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
