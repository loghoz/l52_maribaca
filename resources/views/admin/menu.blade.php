<div class="user-panel">
  <div class="pull-left image">
    <img src="{{ url('itlabil/images/default/avatar4.png')}}" class="img-circle" alt="User Image">
  </div>
  <div class="pull-left info">
    <p>Admin</p>
		<a href="{{ url('/tahun') }}">T.A. {{ Cookie::get('tahun_ajaran') }}</a> 
  </div>
</div>

<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MENU</li>

	{{--  BERANDA  --}}
	<li>
		<a href="{{ url('/admin/beranda') }}">
			<i class="fa fa-home"></i>
			<span>Beranda</span>
		</a>
	</li>

	{{--  MASTER DATA  --}}
	<li class="treeview">
		<a href="#">
    	<i class="fa fa-user"></i> <span>Master Data</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
			<li class="treeview">
        <a href="#"><i class="fa fa-circle-o"></i> Siswa
          <span class="pull-right-container">
          	<i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/admin/siswa') }}"><i class="fa fa-circle-o"></i> Data Siswa</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Pembagian Kelas</a></li>
        </ul>
      </li>
			<li><a href="{{ url('/admin/guru') }}"><i class="fa fa-circle-o"></i>Guru</a></li>
            <!-- <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li> -->
    </ul>
	</li>
	
	{{--  MASTER AKADEMIK  --}}
	<li class="treeview">
		<a href="#">
			<i class="fa fa-file"></i>
			<span>Master Akademik</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{{ url('/admin/jurusan') }}"><i class="fa fa-circle-o"></i> <span>Jurusan</span></a></li>
			<li><a href="{{ url('/admin/kelas') }}"><i class="fa fa-circle-o"></i> <span>Kelas</span></a></li>
			<li class="treeview">
        <a href="#"><i class="fa fa-circle-o"></i> Mata Pelajaran
          <span class="pull-right-container">
          	<i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/admin/mapel/na') }}"><i class="fa fa-circle-o"></i> Tambah MaPel</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Pembagian MaPel</a></li>
        </ul>
      </li>
			<li><a href="{{ url('/admin/akun/admin') }}"><i class="fa fa-circle-o"></i> <span>Akun</span></a></li>
			<li><a href="{{ url('/admin/sampah') }}"><i class="fa fa-circle-o"></i> <span>Kotak Sampah</span></a></li>
		</ul>
	</li>

	{{--  BUKU --}}
	<li>
		<a href="{{ url('/admin/buku') }}">
			<i class="fa fa-book"></i>
			<span>Buku</span>
		</a>
	</li>

	{{--  NILAI --}}
	<li>
		<a href="{{ url('/admin/nilai') }}">
			<i class="fa fa-edit"></i>
			<span>Nilai</span>
		</a>
	</li>

	{{--  PENGATURAN  --}}
	<li class="treeview">
		<a href="#">
			<i class="fa fa-cog"></i>
			<span>Pengaturan</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{{ url('/admin/akun/admin') }}"><i class="fa fa-circle-o"></i> <span>Akun</span></a></li>
			<li><a href="{{ url('/admin/sampah') }}"><i class="fa fa-circle-o"></i> <span>Kotak Sampah</span></a></li>
		</ul>
	</li>

</ul>