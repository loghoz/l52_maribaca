@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Buku
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                      <div class="col-md-4">
                        {!! Form::open(['url' => 'admin/buku', 'method'=>'get', 'class'=>'form-inline'])!!}
                          <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                              {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Nama Buku ....']) !!}
                              {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                          </div>
                          {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                      </div>
                      <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/buku', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <input type="hidden" name="q" value="">
                                {!! Form::submit('Semua Buku', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                          </div>
                      <div class="col-md-4" align="right">
                        <a href="{{ route('admin.buku.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Buku</a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($buku as $item)
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-body box-profile">
                            <img class="img-cover img-responsive" src="{{url('itlabil/images/cover')}}/{{ $item->photo }}" alt="User profile picture">
                            <h4 class="text-judul text-center" title="{{$item->judul_buku}}">{{$item->judul_buku}}</h3>
                            <p class="text-center"><i class="fa fa-fw fa-user"></i>{{$item->guru->nama}}</p>
                            <p class="text-center">
                                <small class="label bg-red"><i class="fa fa-fw fa-black-tie"></i> {{$item->tingkat_kelas}}</small>
                                <small class="label bg-blue"><i class="fa fa-fw fa-book"></i> {{$item->mapel->mata_pelajaran}}</small>
                                <small class="label bg-yellow"><i class="fa fa-fw fa-eye"></i> {{$item->tampil}}</small>
                            </p>
                            <p class="text-center">
                                <div class="col-md-6" align="right">
                                    <a href="{{url('admin/buku/tambahview',$item->id) }}" target="_blank" class="btn btn-primary"><b>Baca</b></a>
                                </div>
                                <div class="col-md-6" align="left">
                                    <a href="{{route('admin.buku.edit',$item->id) }}" class="btn btn-success"><b>Ubah</b></a>
                                </div>
                            </p><br><br>
                            <p class="text-center">
                                <div class="col-md-6" align="right">
                                    <a href="{{route('admin.soal.index') }}" class="btn btn-info"><b>Soal</b></a>
                                </div>
                                <div class="col-md-6" align="left">
                                    {{ Form::open(['route' => ['admin.buku.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-md-12 text-center">
                    @if (isset($q))
                        <h1>:(</h1>
                        <p>buku tidak ditemukan.</p>
                    @endif
                    <p><a href="{{ url('/admin/buku') }}">Lihat semua buku <i class="fa fa-arrow-right"></i></a></p>
                </div>
            @endforelse
        </div>
        {{ $buku->appends(compact('q', 'page'))->links() }}
    </section>
@endsection
