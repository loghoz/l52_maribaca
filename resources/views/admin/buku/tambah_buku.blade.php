@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Buku
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
                <a href="{{ url('/admin/buku') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Data buku</a>
                <br><br>
            </div>

            <div class="col-md-12">
                {!! Form::open(['route' => 'admin.buku.store', 'class'=>'form-horizontal','files'=>true])!!}
                    @include('form._admin_buku')
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
