@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Soal
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
              <a href="{{ route('admin.soal.index') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Data Soal</a>
              <br><br>
            </div>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Soal</h3>
                        </div>
                        <div class="box-body">
                            {!! Form::open(['route' => 'admin.soal.store', 'class'=>'form-horizontal'])!!}
                                @include('form._admin_soal')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

        </div>
    </section>
@endsection
