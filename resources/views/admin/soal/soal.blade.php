@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Soal
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
                <a href="{{ route('admin.soal.create') }}" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah Soal</a>
                <br><br>
            </div>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border">
                            <h3 class="box-title">Data Soal</h3><br><br>
                            Buku : Nama Buku Mata Belajaran<br>
                            Jumlah Soal : 2
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width: 10px">No</th>
                                    <th>Soal</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    <td>1.</td>
                                    <td>Pimpinan dari Gerakan DI/TII Jawa Tengah, saat itu menjabat sebagai ...</td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.soal.show', 1) }}" class="btn btn-primary">Lihat</a>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::model(1, ['route' => ['admin.soal.destroy', 1], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                  {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>Pimpinan dari Gerakan DI/TII Jawa Tengah, saat itu menjabat sebagai ...</td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.soal.show', 1) }}" class="btn btn-primary">Lihat</a>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::model(1, ['route' => ['admin.soal.destroy', 1], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                  {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
