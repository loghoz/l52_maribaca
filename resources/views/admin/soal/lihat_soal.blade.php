@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Soal
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border">
                            <center>
                                <img src="../../itlabil/admin/dist/img/photo1.png" alt="Gambar Soal" class="img-thumbnail" width="500px">
                            </center>
                            <br>
                            1. Pimpinan dari Gerakan DI/TII Jawa Tengah, saat itu menjabat sebagai ...
                        </div>
                        <div class="box-body">
                                <center>
                                    <img src="../../itlabil/admin/dist/img/photo1.png" alt="Gambar Soal" class="img-thumbnail" width="500px">
                                </center>
                                <br>
                                A. Komandan Laskar Hisbullah di front Tulangan, Sidoarjo, dan Mojokerto<br>
                                <hr>
                                <center>
                                    <img src="../../itlabil/admin/dist/img/photo1.png" alt="Gambar Soal" class="img-thumbnail" width="500px">
                                </center>
                                <br>
                                B. Komandan Laskar Hisbullah di front Brebes, Tegal, dan Pekalongan<br>
                                <hr>
                                <center>
                                    <img src="../../itlabil/admin/dist/img/photo1.png" alt="Gambar Soal" class="img-thumbnail" width="500px">
                                </center>
                                <br>
                                C. Komandan Laskar Hisbullah di front Aceh, Jawa Tengah, dan Jawa Barat<br>
                                <hr>
                                <center>
                                    <img src="../../itlabil/admin/dist/img/photo1.png" alt="Gambar Soal" class="img-thumbnail" width="500px">
                                </center>
                                <br>
                                D. Komandan Laskar Hisbullah di frontBrebes,Sidoarjo, dan Mojokerto<br><br>
                        <div>
                        <div class="box-footer">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    Jawaban Benar : A
                                </div>
                                <div class="col-md-6" align="right">
                                    <a href="{{ url('admin/soal') }}" class="btn btn-primary">Kembali</a>
                                    <a href="{{ route('admin.soal.edit', 1) }}" class="btn btn-success">Ubah</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
