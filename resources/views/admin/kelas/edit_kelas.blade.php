@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Kelas
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Kelas</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($kelas, ['route' => ['admin.kelas.update', $kelas],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_kelas', ['model' => $kelas])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
