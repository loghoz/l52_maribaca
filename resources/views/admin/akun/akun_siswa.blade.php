@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Akun
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
              <a href="{{ url('/admin/akun/admin') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Akun Admin</a>
              <a href="{{ url('/admin/akun/guru') }}" class="btn btn-success"><span class="fa fa-eye"></span> Akun Guru</a>
              <br><br>
            </div>
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Akun Siswa</h3>
                    </div>

                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td align="center">No</td>
                                <td align="center">No Induk</td>
                                <td align="center">Password</td>
                                <td colspan="2" align="center">Aksi</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($akun as $item)
                                <tr>
                                    <td align="center">{{ $no++ }}</td>
                                    <td align="center">{{ $item->no_induk }}</td>
                                    <td align="center">*****</td>
                                    <td align="center">
                                        <a href="{{ route('admin.akun.siswa.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $akun->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
