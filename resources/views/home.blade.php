@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div align="center" style="padding-top:20px;">
                <img src="{{asset('itlabil/images/default/logo.png')}}" width="250px" height="250px" alt="logo pelita" />
                <br><br>SMK PELITA PESAWARAN
                <br>LAMPUNG
            </div>
        </div>
    </div>
</div>

@endsection
