@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Beranda
        </h1>
    </section>

    <section class="content">
        <!-- GURU SISWA SAMPAH -->
        <div class="row">

            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                    <h3>{{$jml_siswa}}</h3>

                    <p>Siswa</p>
                    </div>
                    <div class="icon">
                    <i class="fa fa-group"></i>
                    </div>
                    <a href="/admin/siswa" class="small-box-footer">Lihat Data Siswa <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                    <h3>{{$jml_k}}</h3>

                    <p>Kotak Sampah</p>
                    </div>
                    <div class="icon">
                    <i class="fa fa-trash"></i>
                    </div>
                    <a href="/admin/sampah" class="small-box-footer">Lihat Kotak Sampah <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- BUKU POPULER -->
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border">
                            <h3 class="box-title">Buku Populer</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width: 10px">No</th>
                                    <th>Buku</th>
                                    <th>Guru</th>
                                    <th>Pembaca</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($buku as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->judul_buku }}</td>
                                    <td>{{ $item->guru->nama }}</td>
                                    <td>{{ $item->tampil }}</td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.soal.show', 1) }}" class="btn btn-primary">Baca</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- LOG SYSTEM -->
            <!-- <div class="col-md-6">
                <div class="box box-danger">
                    <div class="box-body">
                        <div class="box-header with-border">
                            <h3 class="box-title">Log Sistem</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width: 10px">No</th>
                                    <th>Waktu</th>
                                    <th>Keterangan</th>
                                </tr>
                                <tr>
                                    <td>1.</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
@endsection
