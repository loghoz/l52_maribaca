@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Buku
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Buku</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($buku, ['route' => ['guru.buku.update', $buku],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form._guru_edit_buku', ['model' => $buku])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
