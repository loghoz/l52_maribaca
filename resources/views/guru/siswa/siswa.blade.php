@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Siswa
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Siswa</h3>
                    </div>
                    <div class="box-body">
                      <div class="col-md-4">
                        {!! Form::open(['url' => 'guru/siswa', 'method'=>'get', 'class'=>'form-inline'])!!}
                          <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                              {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Nama Siswa ....']) !!}
                              {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                          </div>
                          {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                      </div>
                      <div class="col-md-4">
                            {!! Form::open(['url' => 'guru/siswa', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <input type="hidden" name="q" value="">
                                {!! Form::submit('Semua Data Siswa', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                          </div>
                      <div class="col-md-4" align="right">
                        
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($siswa as $item)
                @if ($item->jk=='L')
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><img src="../itlabil/images/siswa/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                            <div class="info-box-content">
                                <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}} <i class="fa fa-fw fa-home"></i>{{$item->kelas->kode_kelas}}</span>
                                <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                <div align="right">
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><img src="../itlabil/images/siswa/{{$item->photo}}" class="img-circle" width="70px" alt="User Image"></span>

                            <div class="info-box-content">
                                <span class="info-box-text"><b>{{$item->nama}}</b></span>
                                <span class="info-box-text"><i class="fa fa-fw fa-black-tie"></i>{{$item->no_induk}} <i class="fa fa-fw fa-home"></i>{{$item->kelas->kode_kelas}}</span>
                                <span class="info-box-text"><i class="fa fa-fw fa-phone"></i>{{$item->telp}}</span>
                                <span class="info-box-custom"><i class="fa fa-fw fa-phone"></i>{{$item->user->email}}</span>
                                <div align="right">
                                <br>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
              @empty
              <div class="col-md-12 text-center">
                @if (isset($q))
                  <h1>:(</h1>
                  <p>Siswa tidak ditemukan.</p>
                @endif
                  <p><a href="{{ url('/guru/siswa') }}">Lihat semua data siswa <i class="fa fa-arrow-right"></i></a></p>
              </div>
            @endforelse
        </div>
        {{ $siswa->appends(compact('q', 'page'))->links() }}
    </section>
@endsection
